import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.scene.control.Hyperlink;
import javafx.stage.Stage;

public class CollegeName extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        // Create a VBox as the root node
        VBox root = new VBox(20);
        root.setAlignment(Pos.CENTER);

        // Load the background image
        Image backgroundImage = new Image("\\img\\clgna.jpg");
        BackgroundSize backgroundSize = new BackgroundSize(800, 600, false, false, true, true);
        BackgroundImage background = new BackgroundImage(
                backgroundImage,
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER,
                backgroundSize
        );

        // Set the background of the VBox
        root.setBackground(new Background(background));

        // Create 5 Hyperlinks
        Hyperlink hyperlink1 = createHyperlink("TSSM BSCOER COLLEGE");
        Hyperlink hyperlink2 = createHyperlink("JSPM NTC COLLEGE");
        Hyperlink hyperlink3 = createHyperlink("SINHGAD COLLEGE");
        Hyperlink hyperlink4 = createHyperlink("ZEAL COLLEGE");
        Hyperlink hyperlink5 = createHyperlink("ABHINAV COLLEGE");

        // Add the Hyperlinks to the VBox
        root.getChildren().addAll(hyperlink1, hyperlink2, hyperlink3, hyperlink4, hyperlink5);

        // Create the scene and set it on the stage
        Scene scene = new Scene(root, 800, 600);
        primaryStage.setTitle("JavaFX Background and Hyperlinks");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    // Helper method to create a styled Hyperlink
    private Hyperlink createHyperlink(String text) {
        Hyperlink hyperlink = new Hyperlink(text);
        hyperlink.setFont(Font.font(20));
        hyperlink.setTextFill(Color.BLACK);
        hyperlink.setAlignment(Pos.CENTER);
        hyperlink.setTextAlignment(TextAlignment.CENTER);
        hyperlink.setOnAction(event -> {
            System.out.println("Hyperlink " + text + " clicked!");
            // You can add your desired action for each hyperlink here
        });
        return hyperlink;
    }
}
