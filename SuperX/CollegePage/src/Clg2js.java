import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

public class Clg2js extends Application {
    private TextArea infoTextArea;
    private ImageView imageView;
    private File selectedImageFile;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("College Information Editor");

        infoTextArea = new TextArea();
        infoTextArea.setPromptText("Enter college information here");

        Button addImageButton = new Button("Add Image");
        addImageButton.setOnAction(e -> chooseImage());

        imageView = new ImageView();
        imageView.setFitWidth(200);
        imageView.setFitHeight(200);

        Button displayButton = new Button("Display Information");
        displayButton.setOnAction(e -> displayInformation());

        VBox root = new VBox(10);
        root.getChildren().addAll(infoTextArea, addImageButton, imageView, displayButton);

        Scene scene = new Scene(root, 400, 400);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void chooseImage() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
        selectedImageFile = fileChooser.showOpenDialog(null);

        if (selectedImageFile != null) {
            Image image = new Image(selectedImageFile.toURI().toString());
            imageView.setImage(image);
        }
    }

    private void displayInformation() {
        String collegeInfo = infoTextArea.getText();
        if (selectedImageFile != null) {
            // Process and display the college information and image as needed
            System.out.println("College Information: " + collegeInfo);
            System.out.println("Image File Path: " + selectedImageFile.getAbsolutePath());
        } else {
            System.out.println("Please select an image.");
        }
    }
}


