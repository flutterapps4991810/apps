import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.scene.control.Hyperlink;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundPosition;
//import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;

public class CollegePage extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        // Create a StackPane as the root node
        StackPane root = new StackPane();

        // Load the background image
        Image backgroundImage = new Image("\\img\\clgImg.jpg");
        BackgroundImage background = new BackgroundImage(
                backgroundImage,
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, true, true)
        );

        // Set the background of the StackPane
        root.setBackground(new Background(background));

        // Create a Hyperlink
        Hyperlink hyperlink = new Hyperlink("College");
        hyperlink.setFont(Font.font(100));
        hyperlink.setTextFill(Color.BLUE);

        // Add an action when the hyperlink is clicked
        hyperlink.setOnAction(event -> {
            
            // You can add your desired action here
        });

        // Add the hyperlink to the StackPane
        root.getChildren().add(hyperlink);

        // Create the scene and set it on the stage
        Scene scene = new Scene(root, 800, 600);
        primaryStage.setTitle("JavaFX Background Image with Hyperlink");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
