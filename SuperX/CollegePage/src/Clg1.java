import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Clg1 extends Application {

    @Override
    public void start(Stage primaryStage) {
        // Create a VBox to hold the elements
        VBox root = new VBox(10); // 10 is the spacing between elements

        // College Name and Information Label
        Label collegeInfoLabel = new Label("College Name: My College\nLocation: College Location\nDescription: Some information about the college.");

        // College Image
        Image collegeImage = new Image("img\tssm.jpg"); // Replace with the image path
        ImageView imageView = new ImageView(collegeImage);

        // College Website Hyperlink
        Hyperlink collegeLink = new Hyperlink("Visit College Website");
        collegeLink.setOnAction(e -> {
            // Replace the URL with the actual college website URL
            getHostServices().showDocument("https://tssm.edu.in/");
        });

        // Add elements to the VBox
        root.getChildren().addAll(collegeInfoLabel, imageView, collegeLink);

        // Create a scene
        Scene scene = new Scene(root, 400, 400);

        // Set the stage title
        primaryStage.setTitle("College Information");

        // Set the scene for the stage
        primaryStage.setScene(scene);

        // Show the stage
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
