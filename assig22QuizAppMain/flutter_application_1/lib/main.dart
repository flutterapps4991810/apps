import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return  const MaterialApp(
      home: QuizApp(),
      debugShowCheckedModeBanner: false,
    );
  }
}
class QuizApp extends StatefulWidget {
  const QuizApp({super.key});
  @override
  State createState() => _QuizAppState();
}

class SingleQuestionModel {
  final String? question;
  final List<String>? options;
  final int? answerIndex;

  const SingleQuestionModel({this.question, this.options, this.answerIndex});
}

class _QuizAppState extends State {
  List allQuestion = [
    const SingleQuestionModel(
        question: "Which data structure used for implementing LIFO?",
        options: ["Queue", "Stack", " Tree", " List"],
        answerIndex: 1),
    const SingleQuestionModel(
      question: "Which of the following is a programming language?",
      options: ["Python", " Rails", "Loop", "Safari"],
      answerIndex: 0,
    ),
    const SingleQuestionModel(
      question: "Which of the following is not a web browser?",
      options: ["Chrome", " Safari", " Firefox", "Amazon"],
      answerIndex: 3,
    ),
    const SingleQuestionModel(
      question: "Which of the following is not a data structure?",
      options: ["Stack", "Queue", " Loop", " Tree"],
      answerIndex: 2,
    ),
    const SingleQuestionModel(
      question: "Who invented Dart programming language?",
      options: ["Google", "Amazon", " Safari", "Firefox"],
      answerIndex: 0,
    ),
  ];

  int questionScreen = 0;
  int questionIndex = 0;
  int selectedAnswerIndex = -1;
  int correctAnswers = 0;

  MaterialStateProperty<Color?>? checkAnswer(int buttonIndex) {
    if (selectedAnswerIndex != -1) {
      if (selectedAnswerIndex == buttonIndex) {
        if (selectedAnswerIndex == allQuestion[questionIndex].answerIndex) {
          return const MaterialStatePropertyAll(Colors.green);
        } else {
          return const MaterialStatePropertyAll(Color.fromARGB(255, 232, 60, 48));
        }
      } else {
        if (buttonIndex == allQuestion[questionIndex].answerIndex) {
          return const MaterialStatePropertyAll(Colors.green);
        }
      }
    } else {
      return const MaterialStatePropertyAll(null);
    }
    return null;
  }

  void checkPageValid() {
    if (selectedAnswerIndex == -1) {
      return;
    }
    if (selectedAnswerIndex == allQuestion[questionIndex].answerIndex) {
      correctAnswers++;
    }
    if (selectedAnswerIndex != -1) {
      if (questionIndex == allQuestion.length - 1) {
        questionScreen = 2;
      }
      selectedAnswerIndex = -1;
      setState(() {
        questionIndex++;
      });
    }
  }

  Scaffold isQuestionScreen() {
    if(questionScreen==0){
    return Scaffold(
        /*appBar: AppBar(
          title: const Text(
            "QuizApp",
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w800,
              color: Colors.white,
            ),
          ),
          centerTitle: true,
        ),*/
        body: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [Colors.pink,Colors.black],
              ),
          ),
          
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.network(
                  "https://static.vecteezy.com/system/resources/previews/002/267/735/non_2x/quiz-time-neon-signs-style-text-free-vector.jpg",
                  height: 500,
                  width: 1000,
                ),
                const SizedBox(
                  height: 10,
                ),
                
                const Text(
                  "Let's Play",
                  style: TextStyle(
                    fontSize: 40,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.normal,
                    color: Colors.white,
                  ),
                ),
                const SizedBox(
                  height: 40,
                ),
               
                ElevatedButton(
                  
                    onPressed: () {
                      questionScreen = 1;
                
                      setState(() {});
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.teal,
                      fixedSize: const Size(120, 50),
                    ),
                    
                    child: const Text(
                      'Start Quiz',
                      style: TextStyle(
                        fontSize: 20,
                        fontStyle: FontStyle.italic,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    ),

                    ),
              ],
            ),
          ),
        ),
      );
    }else if(questionScreen == 1) {
      return Scaffold(
        appBar: AppBar(
          title: const Text(
            'QuizApp',
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w800,
              color: Colors.black,
            ),
          ),
          centerTitle: true,
          backgroundColor: const Color.fromARGB(255, 5, 117, 106),
        ),
        body: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [Colors.pink,Colors.black],
              ),
          ),
          child: Column(
            children: [
              const SizedBox(
                height: 25,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    'Question : ',
                    style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.w600,
                        color: Colors.white),
                  ),
                  Text(
                    "${questionIndex + 1}/${allQuestion.length}",
                    style: const TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.w600,
                        color: Colors.white),
                  )
                ],
              ),
              const SizedBox(
                height: 30,
              ),
              SizedBox(
                width: 380,
                height: 50,
                child: Center(
                  child: Text(
                    allQuestion[questionIndex].question,
                    style: const TextStyle(
                        fontSize: 23,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                style: ButtonStyle(
                  
                  backgroundColor: checkAnswer(0),
                ),
                onPressed: () {
                  if (selectedAnswerIndex == -1) {
                    setState(() {
                      selectedAnswerIndex = 0;
                    });
                  }
                },
                child: Text(
                  "A.${allQuestion[questionIndex].options[0]}",
                  style: const TextStyle(
                      fontSize: 20, fontWeight: FontWeight.normal,),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: checkAnswer(1),
                ),
                onPressed: () {
                  if (selectedAnswerIndex == -1) {
                    setState(() {
                      selectedAnswerIndex = 1;
                    });
                  }
                },
                child: Text(
                  "B.${allQuestion[questionIndex].options[1]}",
                  style: const TextStyle(
                      fontSize: 20, fontWeight: FontWeight.normal,),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: checkAnswer(2),
                ),
                onPressed: () {
                  if (selectedAnswerIndex == -1) {
                    setState(() {
                      selectedAnswerIndex = 2;
                    });
                  }
                },
                child: Text(
                  "C.${allQuestion[questionIndex].options[2]}",
                  style: const TextStyle(
                      fontSize: 20, fontWeight: FontWeight.normal),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: checkAnswer(3),
                ),
                onPressed: () {
                  if (selectedAnswerIndex == -1) {
                    setState(() {
                      selectedAnswerIndex = 3;
                    });
                  }
                },
                child: Text(
                  "D.${allQuestion[questionIndex].options[3]}",
                  style: const TextStyle(
                      fontSize: 20, fontWeight: FontWeight.normal),
                ),
              ),
               
            ],
            
          ),
          
        ),
        
        floatingActionButton: FloatingActionButton(
          onPressed: checkPageValid,
          
                    

          backgroundColor: Colors.lightGreen,
          child: const Icon(
            Icons.forward,
            color: Colors.black,
            
          ),
        
        ),
        
        
        
        
      );
     
    } else {
      return Scaffold(
        appBar: AppBar(
          title: const Text(
            "QuizApp",
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w800,
              color: Colors.black,
            ),
          ),
          centerTitle: true,
          backgroundColor: const Color.fromARGB(255, 5, 117, 106),
        ),
        body: Container(
           decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [Colors.pink,Colors.black],
              ),
          ),
          
          child: Center(
            child: Column(
              children: [
                Image.network(
                  "https://www.capgemini.com/gb-en/wp-content/uploads/sites/5/2022/07/Award-cup-getty-image.jpg",
                  height: 500,
                  width: 300,
                ),
                const SizedBox(
                  height: 00,
                ),
                const Text(
                  "Congratulation!! You have passed the test",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Text("$correctAnswers/${allQuestion.length}",
                style: const TextStyle(
                  color: Colors.white,
                ),),
                const SizedBox(
                  height: 25,
                ),
                ElevatedButton(
                    onPressed: () {
                      selectedAnswerIndex = -1;
                      questionIndex = 0;
                      questionScreen = 0;
                      correctAnswers = 0;
                      setState(() {});
                    },
                    child: const Text(
                      "Reset",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    ))
              ],
            ),
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return isQuestionScreen();
  }
}