import 'package:flutter/material.dart';

class selfApp extends StatefulWidget{
   const selfApp({super.key});

  
  State<selfApp>createState()=> _selfAppstate();
}
class _selfAppstate extends State<selfApp>{


  bool showNextScreen=false;
  Widget build(BuildContext context){
  return 
  Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.yellow,
        title: const Text(
          "Tasty Burger",
          style: TextStyle(
            fontStyle: FontStyle.normal,
            color: Colors.black,
            fontSize: 30,
          ),
        ),
      actions: [
        IconButton(onPressed: (){}, icon: Icon(Icons.food_bank_outlined,
        color: Colors.black,)),
      ],
        centerTitle: true,
      ),
       floatingActionButton: FloatingActionButton(
        onPressed: (){
          setState(() {
            showNextScreen=!showNextScreen;
          });
        },
        child: const Text("Next"),
       ),
      body:
        showNextScreen==true ?
        ListView(
        children: [
         Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SingleChildScrollView(scrollDirection: Axis.horizontal,
            child:Row(
              children: [
                
                Container(
                  color: Colors.white,
                  child: Image.network(
                    
                    "https://www.thereciperebel.com/wp-content/uploads/2020/07/best-burgers-www.thereciperebel.com-1200-13-of-18.jpg",

                    
                  ),
                ),
                
              ],
            ),
            ),
          ],
          
         ) 
        ],
      ):


      //1st Screen
      
       ListView(
        children: [
         Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SingleChildScrollView(scrollDirection: Axis.horizontal,
            child:Row(
              children: [
                
                Container(

                  color: Colors.white,
                  child: Image.network(
                    
                    "https://www.thereciperebel.com/wp-content/uploads/2020/07/best-burgers-www.thereciperebel.com-1200-13-of-18.jpg",

                    width: 150,
                    height: 200,
                  ),
                ),
                
                Container(
                  color: Colors.white,
                  child: Column(
                    children: [
                      Container(
                      child  :const Text("King Burger",style: TextStyle(fontSize: 20),),
            
                      ),
                      Container(
                        child: const Text("Egg, Onion, Sauce"),
                      ),
                      Container(
                        child: const Text("Garlic, Medium Ground Beef"),
                      ),

                    ],
                  ),
                ),
                
                
              ],
            ),
            ),
            SizedBox(
              height: 20,
            ),
            
            Text("Special Offer",style: TextStyle(fontSize: 20),),
            SingleChildScrollView( scrollDirection: Axis.horizontal,
            child:Row(
              children: [
                Container(
                  color: Colors.white,
                  child: Image.network(
                    "https://tse4.mm.bing.net/th?id=OIP.H07SGPpx-swRy0WfmnDIdwHaHa&pid=Api&P=0&h=180",

                    width: 150,
                    height: 220,
                  ),
                ),
                Container(
                  color: Colors.white,
                  child: Image.network(
                    "https://tse1.mm.bing.net/th?id=OIP.1vbkxdMKgat-3BtEBaJu0wHaGL&pid=Api&P=0&h=180",

                    width: 150,
                    height: 220,
                  ),
                ),
                Container(
                  color: Colors.white,
                  child: Image.network(
                    "https://tse4.mm.bing.net/th?id=OIP.-41swG7a3L_mAfI5uGYffgAAAA&pid=Api&P=0&h=180",

                    width: 150,
                    height: 220,
                  ),
                ),
                
              ],
            ),
            ),
            SingleChildScrollView(scrollDirection: Axis.horizontal,
            child:Row(
              children: [
                Container(
                  color: Colors.white,
                  child: Image.network(
                    "https://tse4.mm.bing.net/th?id=OIP.xzc4oYn4k-FsEYIdi0cdZQHaE7&pid=Api&P=0&h=180",

                    width: 200,
                    height: 200,
                  ),
                ),
                Column(children: [
                Container(
                  color: Colors.white,
                  child: const Text("Beef Burger",style: TextStyle(fontSize: 20),),
                  ),
                  Container(
                        child: const Text("Egg, Onion, Sauce"),
                  ),
                      Container(
                        child: const Text("Garlic, Medium Ground Beef"),
                  ),
                  ],)
              
                
                
              ],
            ),
            ),
          ],
          
         ) 
        ],
      ),
    );
  }
}