import 'package:flutter/material.dart';
 class assignment1 extends StatefulWidget {

 const assignment1({super.key});

  @override
  State<assignment1> createState() => _assignment1();
}

class _assignment1 extends State<assignment1> {
  List<int> numList = [];
  int counter = 0;
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("List Builder Demo"),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            counter++;
            numList.add(counter);
          });
        },
        child:const Text(
          "+"
        ),
      ),
      body: ListView.builder(
        itemCount: numList.length,
        itemBuilder: (context, index) {
          int content = numList[index];
          return Container(
            height: 50,
            width: 150,
            color: Colors.amber,
            alignment: Alignment.center,
            margin: const EdgeInsets.all(15),
            child: Text(
              "$content",
            ),
          );
        },
      ),
    );
  }
}