import 'package:flutter/material.dart';

class Assignment1 extends StatelessWidget{
  const Assignment1({super.key});

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Center(
        child: Container(
          padding: EdgeInsets.all(20),
          height: 300,
          width: 300,
          decoration: BoxDecoration(border: Border.all(color: Colors.black),),
          child: Center(
            child: Image.network('https://tse1.mm.bing.net/th?id=OIP.L4nUSvQ7ZaefejVVEkLG5QHaEp&pid=Api&P=0&h=180',
            height: 200,
            width: 200,) ,),

        ),
      ),
    );
  }
}