import 'package:flutter/material.dart';

class Assignment4 extends StatelessWidget{
  const Assignment4({super.key});

  @override

  Widget build(BuildContext context){
    return Scaffold(
      body: Center(
        child: Container(
          width: 300,
          height: 200,
          decoration: const BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.black,
                spreadRadius: 0,
                blurRadius: 10,
              offset: Offset(0,-5),
          ),
        ],
       ),
    ),
    ),
    );
  }

}