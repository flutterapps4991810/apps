import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Assignment3 extends StatefulWidget{
  const Assignment3({super.key});

  @override
  _Assignment3State createState() => _Assignment3State();
}
class _Assignment3State extends State<Assignment3>{
  Color borderColor=Colors.red;

  @override
  Widget build(BuildContext context){
    return Scaffold(
     body:Center(
      child:GestureDetector(
        onTap: (){
        setState(() {
          borderColor=Colors.green;
        });
      },
      child: Container(
        width: 200,
        height: 200,
        decoration: BoxDecoration(
          border: Border.all(color: borderColor, width: 4.0),
        ),
      ),
    ),
     ),
    );
  }
} 
