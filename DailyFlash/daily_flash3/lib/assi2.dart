import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Assignment2 extends StatelessWidget{
  const Assignment2({super.key});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Center(
        child: Container(
          height: 300,
          width: 300,
          decoration: const BoxDecoration(
            image:DecorationImage(
            image:NetworkImage('https://tse1.mm.bing.net/th?id=OIP.L4nUSvQ7ZaefejVVEkLG5QHaEp&pid=Api&P=0&h=180'),
            ),
          ),
          child: const Stack(
          children: [
            Center(
              child: Text(
                'Core2Web',
                style: TextStyle(
                  color: Colors.amber,
                  fontSize: 40.0,
                ),
              ),),],
        ),
      ),
    ),
    );
  }
}