import 'package:flutter/material.dart';

class Assignment4 extends StatefulWidget {
  const Assignment4({super.key});

  @override
  _Assignment4State createState() => _Assignment4State();
}

class _Assignment4State extends State<Assignment4> {
  bool isHovered = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: MouseRegion(
          onEnter: (_) => onHover(true),
          onExit: (_) => onHover(false),
          child: FloatingActionButton(
            onPressed: () {},
            backgroundColor: isHovered ? Colors.orange : Colors.blue,
            child: const Icon(Icons.person),
          ),
        ),
      ),
    );
  }

  void onHover(bool hover) {
    setState(() {
      isHovered = hover;
    });
  }
}
