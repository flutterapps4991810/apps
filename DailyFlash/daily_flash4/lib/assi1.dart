import 'package:flutter/material.dart';

class Assignment1 extends StatelessWidget{
  const Assignment1({super.key});

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Center(
        child: ElevatedButton(
          onPressed: (){},
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          elevation: 20,
          shadowColor: Colors.red,
        ), 
        child: const Padding(padding: EdgeInsets.all(15),
        child: Text('Click Me'),
        ),
        ),
      ),
    );
  }
}