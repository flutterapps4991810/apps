import 'package:flutter/material.dart';


class Assignment5 extends StatefulWidget {
  const Assignment5({super.key});

  @override
  _Assignment5State createState() =>
      _Assignment5State();
}

class _Assignment5State
    extends State<Assignment5> {
  Color buttonColor = Colors.blue;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onLongPress: () {
        setState(() {
          buttonColor = Colors.purple;
        });
      },
      child: FloatingActionButton(
        onPressed: () {
          // Add your button press logic here
        },
        backgroundColor: buttonColor,
        child: Icon(Icons.add),
      ),
    );
  }
}
