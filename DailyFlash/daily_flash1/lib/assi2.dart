import 'package:flutter/material.dart';

class Assignment2 extends StatelessWidget{
  const Assignment2({super.key});

  @override 
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 64, 3, 75),
        leading: const Icon(Icons.menu,
        color: Colors.blue,
        ),

        title: const Text("Core2Web",style: TextStyle(color: Colors.amber),),
        centerTitle: true,

        actions: const [
          Icon(Icons.search,
          color: Colors.blue,
          ),
           Icon(Icons.settings,
          color: Colors.blue,
          ),
           Icon(Icons.notifications_none,
          color: Colors.blue,
          ),

        ],
        
      ),
    );
  
  }
}