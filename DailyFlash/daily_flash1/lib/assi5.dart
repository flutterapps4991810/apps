import 'package:flutter/material.dart';

class Assignment5 extends StatelessWidget{
  const Assignment5({super.key});

  @override 
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 64, 3, 75),
      
        title: const Text("Core2Web",style: TextStyle(color: Colors.amber),),
        centerTitle: true,  
      ),
      body: Center(
        child: Container(
          height: 200,
          width: 200,

          decoration: BoxDecoration(
            color: Colors.green,
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: const [
              BoxShadow(
                color: Colors.red,
                blurRadius: 10.0,
                offset: Offset(0, 4),
              ),
            ], ),
        ),
      ),
    );
  
  }
}