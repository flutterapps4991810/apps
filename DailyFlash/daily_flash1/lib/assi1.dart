import 'package:flutter/material.dart';

class Assignment1 extends StatelessWidget{
  const Assignment1({super.key});

  @override 
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 64, 3, 75),
        leading: const Icon(Icons.add_location_alt_outlined,
        color: Colors.blue,
        ),

        title: const Text("Core2Web",style: TextStyle(color: Colors.amber),),
        centerTitle: true,


        actions: const [
          Icon(Icons.account_box_outlined,
          color: Colors.blue,
          ),
        ],
        
      ),
    );
  
  }
}