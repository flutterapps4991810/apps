import 'package:flutter/material.dart';

class Assignment3 extends StatelessWidget{
  const Assignment3({super.key});

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Center(
        child: Container(
          height: 200,
          width: 200,
          
          decoration: BoxDecoration(
            border: Border.all(color: Colors.red),
            borderRadius: const BorderRadius.only(topRight: Radius.circular(20),),
          ),
          child: const  Center(
            child: Text(
              'Hello',
            ),
            ),
        ),
      ),
      
    );

  }
} 