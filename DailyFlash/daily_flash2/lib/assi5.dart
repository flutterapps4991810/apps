import 'package:flutter/material.dart';


class Assignment5 extends StatefulWidget {
  const Assignment5({Key? key}) : super(key: key);

  @override
  _Assignment5State createState() => _Assignment5State();
}

class _Assignment5State extends State<Assignment5> {
  String buttonText = 'Click me!';
  Color containerColor = Colors.red;

  void onTapContainer() {
    setState(() {
      if (buttonText == 'Click me!') {
        buttonText = 'Container Tapped';
        containerColor = Colors.blue;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: GestureDetector(
          onTap: onTapContainer,
          child: Container(
            height: 200,
            width: 200,
            color: containerColor,
            child: Center(
              child: Text(
                buttonText,
                style: const TextStyle(
                  color: Colors.white, // Text color when container is tapped
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
