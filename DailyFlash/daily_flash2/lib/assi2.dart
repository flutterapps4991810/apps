import 'package:flutter/material.dart';

class Assignment2 extends StatelessWidget{
  const Assignment2({super.key});

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Center(
        child: Container(
          
          height: 100,
          width: 100,
        
          decoration: const BoxDecoration(
            color: Colors.amber,
            border: Border(
              left: BorderSide(color: Colors.black, width: 5,),
            ),
          ),
          child: const Text('Hello'),
          padding: const EdgeInsets.all(15),
        ),
      ),
          
      
    );

  }
} 