import 'package:flutter/material.dart';

class Assignment1 extends StatelessWidget{
  const Assignment1({super.key});

@override 
Widget build(BuildContext context){
  return Scaffold(
    appBar: AppBar(
      backgroundColor: Colors.blue,
      title: const Text(
        "ContainerApp",
      ),
    ),
       body:
        Container(
          height: 200,
          width: 200,
          color: Colors.red,
        ),
  );
}
}