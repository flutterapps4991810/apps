import 'package:flutter/material.dart';

class Assignment1 extends StatelessWidget{
  const Assignment1({super.key});

@override 
Widget build(BuildContext context){
  return Scaffold(
    appBar: AppBar(
      backgroundColor: Colors.blue,
      title: const Text(
        "ContainerApp",
      ),
    ),
       body:
        Container(
          height: 300,
          width: 300,decoration: BoxDecoration(
            border: Border.all(
              color: Colors.yellow,
              width: 5,
            )
          ),
        ),
  );
}
}