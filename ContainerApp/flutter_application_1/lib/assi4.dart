import 'package:flutter/material.dart';

class Assignment1 extends StatelessWidget{
  const Assignment1({super.key});

@override 
Widget build(BuildContext context){
  return Scaffold(
    appBar: AppBar(
      backgroundColor: Colors.blue,
      title: const Text(
        "ContainerApp",
      ),
    ),
       body:
        Container(
          margin: const EdgeInsets.only(top:10, bottom: 10, left:10),
          color: Colors.purple,
        ),
  );
}
}