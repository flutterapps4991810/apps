import 'package:flutter/material.dart';

class Assignment1 extends StatelessWidget{
  const Assignment1({super.key});

@override 
Widget build(BuildContext context){
  return Scaffold(
    appBar: AppBar(
      backgroundColor: Colors.blue,
      title: const Text(
        "ContainerApp",
      ),
    ),
       body:
        Container(
          padding: const EdgeInsets.only(left: 10, right: 10, top:10, bottom: 10),
          height: 200,
          width: 200,
          color: Colors.blue,
          
          child: Container(
            color: Colors.purple,
          ),
          ),
        
        
  );
}
}