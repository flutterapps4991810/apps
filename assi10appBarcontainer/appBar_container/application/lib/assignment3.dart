import 'package:flutter/material.dart';

class Assignment3 extends StatelessWidget{
  const Assignment3({super.key});

@override 
Widget build(BuildContext context){
  return Scaffold(
    appBar: AppBar(
      backgroundColor: Colors.deepPurple,
      title: const Text(

        "HelloWorld",
      ),
      centerTitle: true,
    ),
    body: Center(
      child: 
        Container(
          height:150,
          width:150,
          color: Colors.blue,
        ),
      
      ),
  );
}
}