import 'package:flutter/material.dart';

class Assignment2 extends StatelessWidget{
  const Assignment2({super.key});

@override 
Widget build(BuildContext context){
  return Scaffold(
    appBar: AppBar(
      backgroundColor: Colors.blue,
      title: const Text(

        "Core2Web",
      ),
      centerTitle: true,
      actions: [
         const Icon(
          Icons.favorite_outline_rounded,
          color: Colors.red,
         ),
         const Icon(
          Icons.comment_bank_rounded,
        ),
        
      
      ],

    ),
  );
}
}