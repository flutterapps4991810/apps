import 'package:flutter/material.dart';

class Assignment6 extends StatelessWidget {
  const Assignment6({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Scrollable Container Screen'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: List.generate(
            10,
            (index) => Container(
              width: double.infinity,
              height: 100,
              margin: EdgeInsets.all(8.0),
              decoration: BoxDecoration(
                color: _generateRandomColor(),
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: Center(
                child: Text(
                  'Container $index',
                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Color _generateRandomColor() {
    return Color((List.generate(3, (index) => index == 0 ? 255 : _getRandomValue())).fold<int>(
      0,
      (previous, value) => (previous << 8) | value,
    ));
  }

  int _getRandomValue() {
    return (40 + (100 * (DateTime.now().microsecondsSinceEpoch % 1000) / 1000)).floor();
  }
}