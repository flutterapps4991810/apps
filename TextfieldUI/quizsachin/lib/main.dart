import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: QuizApp(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class QuizApp extends StatefulWidget {
  const QuizApp({super.key});

  @override
  State createState() => _QuizAppState();
}

class SingleQuestionIndex {
  final String? question;
  final List<String>? options;
  final int? answerIndex;
  SingleQuestionIndex({this.question, this.options, this.answerIndex});
}

class _QuizAppState extends State {
  List allQuestions = [
    SingleQuestionIndex(
      question: "1. Which country hosted the ICC Cricket World Cup 2023?",
      options: ["India", "Australia", "England", "South Africa"],
      answerIndex: 0,
    ),
    SingleQuestionIndex(
      question: "2. Who won the ICC Cricket World Cup 2023?",
      options: ["India", " New Zealand", "Australia", "South Africa"],
      answerIndex: 2,
    ),
    SingleQuestionIndex(
      question:
          "3. Who was awarded the Player of the Tournament in the ICC Cricket World Cup 2023?",
      options: [
        "Virat Kohli (India)",
        "Kane Williamson (New Zealand)",
        "Babar Azam (Pakistan)",
        "Joe Root (England)"
      ],
      answerIndex: 0,
    ),
    SingleQuestionIndex(
      question:
          "4. How many teams participated in the ICC Cricket World Cup 2023?",
      options: ["8", "10", "16", "11"],
      answerIndex: 1,
    ),
    SingleQuestionIndex(
      question:
          "5. Which player scored the highest individual score in the ICC Cricket World Cup 2023?",
      options: [
        "David Warner (Australia)",
        "Rohit Sharma (India)",
        "Glenn Maxwell(Australia)",
        "Kane Williamson (New Zealand)"
      ],
      answerIndex: 2,
    ),
  ];

  int questionScreen = 0;
  int questionIndex = 0;
  int selectedAnswerIndex = -1;
  int noofCorrectAnswer = 0;

  // ignore: non_constant_identifier_names
  MaterialStateProperty<Color?> CheckAnswer(int buttonIndex) {
    if (selectedAnswerIndex != -1) {
      if (buttonIndex == allQuestions[questionIndex].answerIndex) {
        return const MaterialStatePropertyAll(Colors.green);
      } else if (buttonIndex == selectedAnswerIndex) {
        return const MaterialStatePropertyAll(Colors.red);
      } else {
        return const MaterialStatePropertyAll(null);
      }
    } else {
      return const MaterialStatePropertyAll(null);
    }
  }

  // ignore: non_constant_identifier_names
  void ValidityCurrentPage() {
    if (selectedAnswerIndex == -1) {
      return;
    }
    if (selectedAnswerIndex == allQuestions[questionIndex].answerIndex) {
      noofCorrectAnswer += 1;
    }
    if (selectedAnswerIndex != -1)
    // ignore: curly_braces_in_flow_control_structures
    if (questionIndex == allQuestions.length - 1) {
      setState(() {
        questionScreen = 2;
      });
    }
    {
      selectedAnswerIndex = -1;
      setState(() {
        questionIndex += 1;
      });
    }
  }

  Scaffold isQuestionScreen() {
    if(questionScreen==0){
    return Scaffold(
        
        body: 
         Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [Colors.pink, Colors.purple,Colors.black],
              ),
          ),
       child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
             
              Image.network(
                "https://tse2.mm.bing.net/th?id=OIP.UmmA_EHKUcT68No29HFvlAHaDB&pid=Api&P=0&h=180",
                height: 350,
                width: 400,
              ),
              const Text(
                "Let's start the Quiz.....!",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 28,
                  fontWeight: FontWeight.w500,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                width: 200,
                child: ElevatedButton(
                  onPressed: () {
                     questionScreen = 1;
                   
                    setState(() {});
                  },
                   style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.teal,
                      fixedSize: const Size(120, 50),
                    ),
                  child: const Text(
                    "Start Quiz",
                    style:
                        TextStyle(color: Colors.black,fontSize: 28, fontWeight: FontWeight.normal),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
    
    }else if(questionScreen == 1) {
      return Scaffold(
        appBar: AppBar(
          title: const Text(
            "QuizApp",
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w800,
              color: Colors.black,
            ),
          ),
          centerTitle: true,
          backgroundColor: const Color.fromARGB(255, 2, 94, 94),
        ),
        body: 
         Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [Colors.pink,Colors.purple,Colors.black],
              ),
          ),
          child: Column(
          children: [
            const SizedBox(
              height: 25,
            ),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              const Text(
                "Questions: ",
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.w600,
                ),
              ),
              Text(
                "${questionIndex + 1}/${allQuestions.length}",
                style: const TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ]),
            const SizedBox(
              height: 50,
            ),
            SizedBox(
              width: 380,
              height: 50,
              child: Text(
                allQuestions[questionIndex].question,
                style: const TextStyle(
                  fontSize: 23,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            SizedBox(
              width: 200,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: CheckAnswer(0),
                ),
                onPressed: () {
                  if (selectedAnswerIndex == -1) {
                    setState(() {
                      selectedAnswerIndex = 0;
                    });
                  }
                },
                child: Text(
                  "A.${allQuestions[questionIndex].options[0]}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: 200,
              child: SizedBox(
                child: ElevatedButton(
                  style: ButtonStyle(backgroundColor: CheckAnswer(1)),
                  onPressed: () {
                    if (selectedAnswerIndex == -1) {
                      setState(() {
                        selectedAnswerIndex = 1;
                      });
                    }
                  },
                  child: Text(
                    "B.${allQuestions[questionIndex].options[1]}",
                    style: const TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: 200,
              child: ElevatedButton(
                style: ButtonStyle(backgroundColor: CheckAnswer(2)),
                onPressed: () {
                  if (selectedAnswerIndex == -1) {
                    setState(() {
                      selectedAnswerIndex = 2;
                    });
                  }
                },
                child: Text(
                  "C.${allQuestions[questionIndex].options[2]}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: 200,
              child: ElevatedButton(
                style: ButtonStyle(backgroundColor: CheckAnswer(3)),
                onPressed: () {
                  if (selectedAnswerIndex == -1) {
                    setState(() {
                      selectedAnswerIndex = 3;
                    });
                  }
                },
                child: Text(
                  "D.${allQuestions[questionIndex].options[3]}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            ValidityCurrentPage();
          },
          backgroundColor:const  Color.fromARGB(255, 2, 108, 18),
          child: const Icon(
            Icons.forward,
            color: Colors.black,
          ),
        ),
      );
    } else {
      return Scaffold(
        
        body:  Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [Colors.pink,Colors.purple,Colors.black],
              ),
          ),
        
          child: Center(
            child: Column(children: [
              Image.network(
                "https://t4.ftcdn.net/jpg/04/38/37/93/360_F_438379312_GLpsYoC2ffr5euMSbiYxu66z1AjQb3Qh.jpg",
                height: 350,
                width: 400,
              ),
              const Text(
                "Congratulations",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 45,
                  fontWeight: FontWeight.w500,
                ),
              ),
              const SizedBox(
                height: 35,
              ),
              const Text(
                "You have completed the quiz",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 23,
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Text("$noofCorrectAnswer/${allQuestions.length}"),
              
              const SizedBox(
                height:20,
              ),
              SizedBox(
                width: 200,
                child: ElevatedButton(
                  onPressed: () {
                    questionIndex = 0;
                    questionScreen = 0;
                    noofCorrectAnswer = 0;
                    selectedAnswerIndex = -1;
                    setState(() {});
                  },
                  child: const Text(
                    "Reset",
                    
                    style:
                        TextStyle(fontSize: 35, fontWeight: FontWeight.normal,color: Colors.black),
                  )),
              ),
            ],
            ),
          ),
      ),
      );
    }
  }



  @override
  Widget build(BuildContext context) {
    return isQuestionScreen();
  }
}