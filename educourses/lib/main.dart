import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: UiDesignApp(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class UiDesignApp extends StatefulWidget {
  @override
  State createState() => _UiDesignApp();
}

class _UiDesignApp extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(205, 218, 218, 1),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: 47, left: 20, right: 20),
            child: Row(
              children: [
                IconButton(
                  onPressed: () {},
                  icon: Icon(Icons.menu, size: 30),
                ),
                Spacer(),
                IconButton(
                  onPressed: () {},
                  icon: Icon(Icons.notifications_active_outlined, size: 30),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 45, left: 30),
            child: Row(
              children: [
                Text(
                  "Welcome to New",
                  style: GoogleFonts.jost(
                    textStyle:
                        TextStyle(fontSize: 27, fontWeight: FontWeight.w300),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 30),
            child: Row(
              children: [
                Text(
                  "Educourse",
                  style: GoogleFonts.jost(
                    textStyle:
                        TextStyle(fontSize: 37, fontWeight: FontWeight.w700),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 34, left: 30, right: 30),
            child: Column(
              children: [
                TextField(
                  maxLines: 1,
                  decoration: InputDecoration(
                    fillColor: Colors.white,
                    filled: true,
                    suffixIcon: IconButton(
                      onPressed: () => {},
                      icon: Icon(
                        Icons.search,
                        size: 27,
                      ),
                    ),
                    suffixIconColor: Colors.black,
                    hintText: "Enter your Keyword",
                    hintStyle: GoogleFonts.inter(
                        textStyle: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.w400)),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(100),
                      borderSide: BorderSide.none,
                    ),
                  ),
                ),
              ],
            ),
          ),
        Padding(padding: EdgeInsets.all(10),
        child: Container(
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(10),
        ),),
        )
        ],
      ),
    );
  }
}