import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Second extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        // Create a StackPane to hold the components and center them
        StackPane root = new StackPane();

        // Set the background image
        Image backgroundImage = new Image("your_background_image.jpg");
        ImageView backgroundImageView = new ImageView(backgroundImage);
        root.getChildren().add(backgroundImageView);

        // Create a VBox to hold the buttons in the center
        VBox buttonContainer = new VBox(20);
        buttonContainer.setAlignment(Pos.CENTER);

        // Create and configure four clickable buttons with a glossy violet color
        for (int i = 1; i <= 4; i++) {
            Button button = new Button("Button " + i);
            button.setStyle(
                "-fx-background-color: linear-gradient(to bottom, #9933CC, #660099);" +
                "-fx-text-fill: white;" +
                "-fx-font-size: 18px;" +
                "-fx-padding: 10px 20px;"
            );
            button.setOnAction(e -> extracted2(i));
            buttonContainer.getChildren().add(button);
        }

        // Add the VBox with buttons to the StackPane
        root.getChildren().add(buttonContainer);

        // Create the scene and set it on the stage
        Scene scene = new Scene(root, 800, 600);
        primaryStage.setTitle("JavaFX Page Example");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void extracted2(int i) {
        extracted(i);
    }

    private void extracted(int i) {
        // Handle button click action here
        System.out.println("Button " + i + " Clicked");
    }
}
