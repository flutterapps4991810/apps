import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class First extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        // Create a StackPane to hold the components
        StackPane root = new StackPane();

        // Set the background image
        Image backgroundImage = new Image("City.jpg");
        ImageView backgroundImageView = new ImageView(backgroundImage);
        root.getChildren().add(backgroundImageView);

        // Create a large clickable button with a glossy background
        Button largeButton = new Button("IIT Narhe");
        largeButton.setStyle("-fx-background-color: linear-gradient(to bottom, #b8e2fc, #a4ceee); -fx-font-size: 50px;");
        largeButton.setOnMouseClicked(e -> {
            // Handle large button click action here
            System.out.println("Large Button Clicked");
        });

        // Create a small clickable button at the right-side bottom
        Button smallButton = new Button("About");
        smallButton.setStyle("-fx-font-size: 12px;");
        smallButton.setOnMouseClicked(e -> {
            // Handle small button click action here
            System.out.println("Small Button Clicked");
        });

        // Add the large button to the center of the StackPane
        root.getChildren().add(largeButton);

        // Create an HBox for the small button at the right-side bottom
        HBox buttonBox = new HBox(smallButton);
        buttonBox.setAlignment(Pos.BOTTOM_RIGHT);

        // Add the buttonBox to the StackPane
        root.getChildren().add(buttonBox);

        // Create the scene and set it on the stage
        Scene scene = new Scene(root, 800, 600);
        primaryStage.setTitle("JavaFX Page Example");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
