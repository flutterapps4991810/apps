import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class Third extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        // Create a StackPane to hold the components
        StackPane root = new StackPane();

        // Set the background image
        Image backgroundImage = new Image("City.jpg");
        ImageView backgroundImageView = new ImageView(backgroundImage);
        root.getChildren().add(backgroundImageView);

        // Create one large clickable button
        Button largeButton = new Button("Large Button");
        largeButton.setStyle(
            "-fx-background-color: linear-gradient(to bottom, #FF66B2, #FF3399);" +
            "-fx-text-fill: white;" +
            "-fx-font-size: 24px;" +
            "-fx-padding: 15px 30px;"
        );
        largeButton.setOnAction(e -> {
            // Handle large button click action here
            System.out.println("Large Button Clicked");
        });

        // Create five clickable buttons
        Button button1 = new Button("Button 1");
        Button button2 = new Button("Button 2");
        Button button3 = new Button("Button 3");
        Button button4 = new Button("Button 4");
        Button button5 = new Button("Button 5");

        // Style the smaller buttons
        button1.setStyle(
            "-fx-background-color: linear-gradient(to bottom, #66FF99, #33CC66);" +
            "-fx-text-fill: white;" +
            "-fx-font-size: 18px;" +
            "-fx-padding: 10px 20px;"
        );
        button2.setStyle(button1.getStyle());
        button3.setStyle(button1.getStyle());
        button4.setStyle(button1.getStyle());
        button5.setStyle(button1.getStyle());

        // Set actions for the smaller buttons
        button1.setOnAction(e -> {
            System.out.println("Button 1 Clicked");
        });
        button2.setOnAction(e -> {
            System.out.println("Button 2 Clicked");
        });
        button3.setOnAction(e -> {
            System.out.println("Button 3 Clicked");
        });
        button4.setOnAction(e -> {
            System.out.println("Button 4 Clicked");
        });
        button5.setOnAction(e -> {
            System.out.println("Button 5 Clicked");
        });

        // Add the large button and the smaller buttons to the StackPane
        root.getChildren().addAll(largeButton, button1, button2, button3, button4, button5);

        // Create the scene and set it on the stage
        Scene scene = new Scene(root, 800, 600);
        primaryStage.setTitle("JavaFX Page Example");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
