import 'package:flutter/material.dart';


class Assignment8 extends StatefulWidget{
  const Assignment8({super.key});

  @override 
  State<Assignment8> createState() => _Assignment8State();
}

class _Assignment8State extends State<Assignment8>{

  bool _isPost1Liked=false;
  bool _isPost2Liked=false;
  bool _isPost3Liked=false;


  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Text(
          "Instagram",
          style: TextStyle(
            fontStyle: FontStyle.italic,
            color: Colors.black,
            fontSize: 30,
          ),
        ),
        actions: const [
          Icon(
            Icons.favorite_rounded,
            color: Colors.red,

          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
      
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
              color: Colors.black,
              child: Image.network(
                "https://media.istockphoto.com/id/1038412058/photo/pacific-ocean-at-big-sur.jpg?s=2048x2048&w=is&k=20&c=rIYJPy1l5Lv_m0BL1FOIOq8eF-aFG4lMI6ExkD88MME=",


                width: double.infinity,
                height: 200,

              ),
              ),
              Row(
                children: [
                  IconButton(
                    onPressed: () {
                      setState((){
                        _isPost1Liked= !_isPost1Liked;
                      });

                    },
                    icon: _isPost1Liked
                          ? const Icon(
                            Icons.favorite_rounded,
                            color: Colors.red,
                          )
                    : const Icon(
                      Icons.favorite_outline_rounded,
                    ),
                     // Icons.favorite_outline_outlined,

                    ),
                  
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.comment_outlined,
                  ),
                ),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.send,

                  ),
                ),
                /*const SizedBox(
                  width: 200,

                ),*/
                const Spacer(),
                IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.bookmark_outline_outlined,
                      

                    ),
                  ),
              ],
              ),
            ],
          ),
          /* Column(

            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
              color: Colors.black,
              child: Image.network(
                "https://media.istockphoto.com/id/1038412058/photo/pacific-ocean-at-big-sur.jpg?s=2048x2048&w=is&k=20&c=rIYJPy1l5Lv_m0BL1FOIOq8eF-aFG4lMI6ExkD88MME=",


                width: double.infinity,
                height: 200,

              ),
              ),
              Row(
                children: [
                  IconButton(           start from here
                    onPressed: () {
                      setState((){
                        _isPost2Liked = !-isPost2Liked;
                      
                    });
                    },
                    
                    icon: Icon(
                      _isPost2Liked
                      ? Icons.favorite_rounded
                      : Icons.favorite_outline_outlined,
                      color: _isPost2Liked ? Colors:red: Colors:white
                  ),
                ),   2nd method for liking the post
                IconButton(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.send,

                  ),
                ),
              ],
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
              color: Colors.black,
              child: Image.network(
                "https://media.istockphoto.com/id/1038412058/photo/pacific-ocean-at-big-sur.jpg?s=2048x2048&w=is&k=20&c=rIYJPy1l5Lv_m0BL1FOIOq8eF-aFG4lMI6ExkD88MME=",


                width: double.infinity,
                height: 200,

              ),
              ),
              Row(
                children: [
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.favorite_outline_outlined,
                    ),
                  ),
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.favorite_outline_outlined,
                  ),
                ),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.send,

                  ),
                ),
              ],
              ),
            ],
          ), */
        ],
      ),
    ),
    );

  }
 }