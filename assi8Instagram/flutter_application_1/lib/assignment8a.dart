import 'package:flutter/material.dart';


class Assignment5 extends StatefulWidget{
  const Assignment5({super.key});

  @override 
  State<Assignment5> createState() => _Assignment5State();
}

class _Assignment5State extends State<Assignment5>{

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Text(
          "Instagram",
          style: TextStyle(
            fontStyle: FontStyle.italic,
            color: Colors.black,
            fontSize: 30,
          ),
        ),
        actions: const [
          Icon(
            Icons.favorite_rounded,
            color: Colors.red,

          )
        ],
      ),
      body: ListView(
        children: [  Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
              color: Colors.black,
              child: Image.network(
                "https://media.istockphoto.com/id/1038412058/photo/pacific-ocean-at-big-sur.jpg?s=2048x2048&w=is&k=20&c=rIYJPy1l5Lv_m0BL1FOIOq8eF-aFG4lMI6ExkD88MME=",


                width: double.infinity,
                height: 200,

              ),
              ),
              Row(
                children: [
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.favorite_outline_outlined,
                    ),
                  ),
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.favorite_outline_outlined,
                  ),
                ),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.send,

                  ),
                ),
              ],
              ),
            ],
          ),
           Column(

            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
              color: Colors.black,
              child: Image.network(
                "https://media.istockphoto.com/id/1038412058/photo/pacific-ocean-at-big-sur.jpg?s=2048x2048&w=is&k=20&c=rIYJPy1l5Lv_m0BL1FOIOq8eF-aFG4lMI6ExkD88MME=",


                width: double.infinity,
                height: 200,

              ),
              ),
              Row(
                children: [
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.favorite_outline_outlined,
                    ),
                  ),
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.favorite_outline_outlined,
                  ),
                ),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.send,

                  ),
                ),
              ],
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
              color: Colors.black,
              child: Image.network(
                "https://media.istockphoto.com/id/1038412058/photo/pacific-ocean-at-big-sur.jpg?s=2048x2048&w=is&k=20&c=rIYJPy1l5Lv_m0BL1FOIOq8eF-aFG4lMI6ExkD88MME=",


                width: double.infinity,
                height: 200,

              ),
              ),
              Row(
                children: [
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.favorite_outline_outlined,
                    ),
                  ),
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.favorite_outline_outlined,
                  ),
                ),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.send,

                  ),
                ),
              ],
              ),
            ],
          ),
          ],
      ),
    /*  body: SingleChildScrollView(
        child: Column(
      
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
              color: Colors.black,
              child: Image.network(
                "https://media.istockphoto.com/id/1038412058/photo/pacific-ocean-at-big-sur.jpg?s=2048x2048&w=is&k=20&c=rIYJPy1l5Lv_m0BL1FOIOq8eF-aFG4lMI6ExkD88MME=",


                width: double.infinity,
                height: 200,

              ),
              ),
              Row(
                children: [
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.favorite_outline_outlined,
                    ),
                  ),
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.favorite_outline_outlined,
                  ),
                ),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.send,

                  ),
                ),
              ],
              ),
            ],
          ),
           Column(

            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
              color: Colors.black,
              child: Image.network(
                "https://media.istockphoto.com/id/1038412058/photo/pacific-ocean-at-big-sur.jpg?s=2048x2048&w=is&k=20&c=rIYJPy1l5Lv_m0BL1FOIOq8eF-aFG4lMI6ExkD88MME=",


                width: double.infinity,
                height: 200,

              ),
              ),
              Row(
                children: [
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.favorite_outline_outlined,
                    ),
                  ),
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.favorite_outline_outlined,
                  ),
                ),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.send,

                  ),
                ),
              ],
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
              color: Colors.black,
              child: Image.network(
                "https://media.istockphoto.com/id/1038412058/photo/pacific-ocean-at-big-sur.jpg?s=2048x2048&w=is&k=20&c=rIYJPy1l5Lv_m0BL1FOIOq8eF-aFG4lMI6ExkD88MME=",


                width: double.infinity,
                height: 200,

              ),
              ),
              Row(
                children: [
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.favorite_outline_outlined,
                    ),
                  ),
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.favorite_outline_outlined,
                  ),
                ),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.send,

                  ),
                ),
              ],
              ),
            ],
          ),
        ],
      ),
    ),*/ 
    );

  }
 }