import 'package:flutter/material.dart';

void main(){
  runApp(const MainApp());
}
class MainApp extends StatelessWidget{
  const MainApp({super.key});
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home:QuizApp(),
      debugShowCheckedModeBanner:false,
    );
  }
}
class QuizApp extends StatefulWidget{
  const QuizApp({super.key});

  @override
  State createState() => _QuizAppState();
}

class _QuizAppState extends State{
  List<Map>allQuestions=[{
    "question":"Who is the founder of Microsoft?",
     "options":["Steve Jobs","Jaff Bezos","Bill Gates","Elon Musk"],
      "answerIndex":2,
  },
  {
    "question":"Who is the founder of Google?",
     "options":["Steve Jobs","Jaff Bezos","Bill Gates","Elon Musk"],
       "answerIndex":0,
  },
  {
    "question":"Who is the founder of Amazon?",
      "options":["Steve Jobs","Jaff Bezos","Bill Gates","Elon Musk"],
       "answerIndex":1,
  },
  {
    "question":"Who is the founder of Tesla?",
      "options":["Steve Jobs","Jaff Bezos","Bill Gates","Elon Musk"],
        "answerIndex":3,
  },
  {
    "question":"Who is the founder of Facebook?",
      "options":["Steve Jobs","Jaff Bezos","Bill Gates","Elon Musk"],
        "answerIndex":3,
  },
  ];

  bool questionScreen=true;
  int questionIndex=0;
  int selectedAnswerIndex=-1;
  int noofCorrectAnswer=0;
  

  MaterialStateProperty<Color?>CheckAnswer(int buttonIndex){
    if(selectedAnswerIndex!=-1){
      if(buttonIndex==allQuestions[questionIndex]["answerIndex"])
      {
        return const MaterialStatePropertyAll(Colors.green);
      }
    
    else if(buttonIndex==selectedAnswerIndex)
    {
      return const MaterialStatePropertyAll(Colors.red);
    }
    else{
      return const MaterialStatePropertyAll(null);
    }
    }
  else{
      return const MaterialStatePropertyAll(null);
    }
  }

  void  ValidityCurrentPage(){
              if(selectedAnswerIndex==-1){
                return;
              }
              if(selectedAnswerIndex==allQuestions[questionIndex]["answerIndex"]){
                noofCorrectAnswer +=1;
              }
              if(selectedAnswerIndex!=-1)
              if(questionIndex==allQuestions.length-1){
                setState(() {
                  questionScreen=false;
                });
              }
            {selectedAnswerIndex=-1;
            setState(() {
              questionIndex+=1;
            });
              }
            }

  Scaffold isQuestionScreen(){
    if(questionScreen==true){
      return Scaffold(
        appBar: AppBar(
          title: const Text("QuizApp",
          style: TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.w800,
            color: Colors.black,
          ),
          ),
          centerTitle: true,
          backgroundColor: Colors.blue,
        ),
        body: Column(
          children: [
            const SizedBox(
              height: 25,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text("Questions: ",
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.w600,
                ),
                  ),
                  Text(
                    "${questionIndex+1}/${allQuestions.length}",
                    style: const TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
              ]
            ),
            const SizedBox(height: 50,),
            SizedBox(
              width: 380,
              height: 50,
              child: Text(
                allQuestions[questionIndex]["question"],
                style: const TextStyle(
                  fontSize: 23,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
            const SizedBox(height: 30,),
            ElevatedButton(style: ButtonStyle(backgroundColor: CheckAnswer(0),
            ),
              onPressed: (){
              if(selectedAnswerIndex==-1){
                  setState(() {
                    selectedAnswerIndex=0;
                  });
                }
            },
             child: Text(
              "A.${allQuestions[questionIndex]["options"][0]}",
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.normal,
              ),
              ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(style: ButtonStyle(backgroundColor: CheckAnswer(1)),
                
                onPressed: (){
                if(selectedAnswerIndex==-1){
                  setState(() {
                    selectedAnswerIndex=1;
                  });
                }
              },
             child: Text(
              "B.${allQuestions[questionIndex]["options"][1]}",
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.normal,
              ),
              ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(style: ButtonStyle(backgroundColor: CheckAnswer(2)),
                onPressed: (){
                if(selectedAnswerIndex==-1){
                  setState(() {
                    selectedAnswerIndex=2;
                  });
                }
              },
             child: Text(
              "C.${allQuestions[questionIndex]["options"][2]}",
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.normal,
              ),
              ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(style: ButtonStyle(backgroundColor: CheckAnswer(3)),
                onPressed: (){
                if(selectedAnswerIndex==-1){
                  setState(() {
                    selectedAnswerIndex=3;
                  });
                }
              },
             child: Text(
              "D.${allQuestions[questionIndex]["options"][3]}",
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.normal,
              ),
              ),
              ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            ValidityCurrentPage();
          },
          backgroundColor: Colors.blue,
          child: const Icon(
            Icons.forward,
            color: Colors.black,
          ),
          ),
      );
    }
    else{
      return  Scaffold(
        appBar:AppBar(
          title: const Text("QuizApp",
          style: TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.w800,
          ),
          ),
        ),
        body: Column(
          children: [
            const SizedBox(
              height: 30,
            ),
            Image.network("data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxIQEhUTEhMVFRAWEhYZGBYYFxUWFRcZFhUWFxUXFxUZHSggGBolHxUVITEiJSkrLi4uFyAzODMtNygtLisBCgoKDg0OGxAQGy0lICUtLS0tLS4tLTAtKy0tLS0tLS0tLS0tLS0tLi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOAA4QMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAABAUDBgcCAQj/xABFEAACAQIDAwkEBAwGAwEAAAABAgADEQQSIQUxQQYTIjJRYXGBoQeRscFCUnKyFBUjM0NTYpKi0eHwY4Kz0uLxVHPCFv/EABsBAQADAQEBAQAAAAAAAAAAAAABAgQDBQYH/8QANxEAAgECAwQIBQMDBQAAAAAAAAECAxEEITEFEkFRE2FxgZGhsfAiMsHR4RQjYkJS8QYVkpPS/9oADAMBAAIRAxEAPwDuMREAREQBERAEREAREQBEgjaCc/zAuXFIVDp0QpYqLntJB90nSE7kuLja/HMRMJrLmyXGci+W+tt17TIzAC50EnQre+hgwzHpA/RcjyNmHxtJMj4eor3ZTcE2vwNtNPUeUkTnS+VWd1w7OHlYtLURMag663107tBp8T5zJOhAiJhr11QXZgova57TBDds2ZonwG8+wSIiIAiIgCIiAIiIAiIgCIiAIkbFYlKeUubBnVB9pzZR5nSSYFmJW4PC1lrVXesXpOV5umQBzdlsbHjc/DvMspGrYymjBWYBjawPG5sPWSr8Cd/dT6+z2u4kxESCCOMOucvlHOFQpa3SKgkgE8QCT7z2yREQDFza3zWGa1r21t2X7JhxKqzKjC4IJsdxy2tft3+klzC9O5U/VJ9QRb1HulKi3o27PC6v5XJjk7ntFAFgLAbgN09xIG18S1Kk7ouZgp4gW062u+3ZJk1GN3wJjFykorVk+JVbBx1SvSzVFCsGy2F+AGpBGl77tdJaxCanFSWjJqQcJOMtUIiJYoIiRted7gnqx/4ysnawJMREsBERAEREAREQBERAERK3bVOu1Fhh2CViNGNvmp18pDdlctCO9JJu1+L0Xafdr4M1VUL1kq06gvuJpurZfMAi/C88YjbNJDk6bVD9BKdR38wF0HebCTcNmCqHIL2GYgFQTbUgEmw8zM8hrii0ZLSaulfR217mQ8NjkdilyKigFkbRwG6ptxBsRcaXBG8GS8shVsJeslUGxVHQ96tlPoyD3mTpKvxKyUcrf4d3+BERJKiJjUG5udNLC27t14/0mSQgIniotwRci43jePCeaFLKoFybcSbmLu9rEmWfCLz7EkgRMVXNplt1ulfssd3fe0yxcCR8bVKoSOtoB4kgD1MkT4ReVmm4tIERsFexDuG4kNv8VN19JICWFgTe2/efGZIkKnGN7L37/IPFIEAAm5tv7e+e4iWSsBERJAiIgCIiAIiV71Korqtr0GpMSbaq6strm+4gnT9mSlchuxYRESCRERAEREAr9r161OmWoUhUqXHRLZRbib90nLu1nqUmN5S4elUFLMXqlguVRexJAsTuG+UqVIwV5OxeMXJbsVnn7fUv83LuJGXFX4QlfMSBpYysa0JfK7kbrRJiYGpMfpke6YKmEfhWce6JVJLSLfevqwop8fUnRKs0XH6Zz7p851x9Mn3TksS+MGu9fcv0fJ+paxKwYt7b9fCRq22XQgFFYE20JU+oMn9VTWrsFSk9C4Ym4sLjib2t/OZJr+C5Q87iTQFJwBTLFzYgdW17aAakaE6zYJ2hJSV4u5FSlOm0pq10n3MRMOIZgOgoY33FsotxN7GZRLnK5GxdXKAdcvEjeOye8Pu6xYcCZnnlRbdunBU5dJvXy5e3a3arp8eBa+R6iJEqY+irZGq01f6pdQ3uJvNCTehVtLUzoSd4tqfjoZknwGfZBIiIgCJB2pi+ZTnLXVWXPwshYB38FBLHuUyTVqqilmIVQLkkgADtJO6CbOyZliYKOIR7lGVgN+Ug20vw8Znggi4muyWyoXJ7LADxMy0ibDNYNbW268yzHVqBQWO4An3Tnu2k5OWXLKy69L+ZN8rWI2OxBAshGfQ67rXFx42vKqtj+Oc+IJtI2OrvUKUl69ZjfuUat5fIGbFhcIlNQoA3bzvMwLfxTbi7LLn3ZX5a9xoaVNZ6kDAbT1Cubg7m7+wzm2wW57GI31qzP95/lNv5autJTk0bKSbaa8D3GajyRpBq4BuBkfcbEXUroeB6U86vUnvdDN33Xr4fnxtnY30IRVN1FxR0wtZSe7+gknDLv8ZXVX6IHaw9NflLPDT1MIsjzqhInh57nhptlocURqki1JMqCRKgmOaO0TCDK3au4HsMsTvlftTqny+MwV/lNFP5j7szEBMSCTZTRe5+yQ/wBkuvtQvrcqnADefEzV9p1CFQg8LeRAB+c2/k4iGkGsC+4niOzwlcPOpVaoRdlm+3T3qWq04wj0jV+B5wuNN9GN+w8Zb0aoYd/ESu2xgBUQsmlRdQRxtrbxkXZOPz5H4OLEd97fEe4zfCpKhNQm8n9cvJ5PtRmcVOO8jYIiRsZjKdFc1R1Re1iB5DtM9K18jM2krsjbSxhR6KD9JWyk9gCO2neSoHmZmfZ1FhY0qZHYUUj4SuGKpYx6Yp5mWlUDl8rBLgMAoZgMxObhfS9+F72Wd42Wj4lItTu9VwMVGiqAKqhVG4AAAeAEyxEodBERAPLC+h3SDV2XRdlZlzBLZVJY01I6pFO+W44G2nCWEQSpNZohHBrzwrDR8hRrbnW91zd6m9jwzMOMmxEBtvUSBth7Um7yB7yL+l5Lq0g1r8GB8xukLbn5rzEzYxtUKj/i/QvS+ddpVbETPiqjH9HRRR4vdvh8ZP2wHFnVGcWscvWHYQOI8P+o/JwflcSe00f9If1l/ONCip4dRfG/r+DpUnu1L9S9Ec+27QepRq1WVgq0z1gRqdOO86yl5G/nif8JvvLN95cG2BreCetRBOf8kTao3/AKz95Z5mKw8aM1FO982ehQqOpRk32G8s9yg8T8JdYTdNdpPdx3KPiZsWE3T0cJoYa2hKMwhSC2pNzcA200AsO7S/mZnmNpvkZkYqgkSoJLeRKpmSodYkSrIO0uqZNryvx56J8J51fRmmnqU2KQutNRvZwo7Lk2EuMFQq0RlKVL7rAH47iO+VGbWl3Vl+9OkSMHhI17ybaasXr1nBKPBkPZ1NlQZhZjqRe9u6/bNTw9dUrYikp1p181rHQOAQAfKbxNRx6AYjEntNH/SP9Js2jCMMNlw08H9jhhmnKW97zX0bRtoN59mOl1R4D4TJPTMgiIgEfC0mUEM5c33kKCO7ogCSIiCW7iIiCBEiYB3KA1BZiTp2amwkuVhLeipc+ZLVnYRImNeoFHNqGbOg13BS4DnyUk+UlywtlcSv2rY02W/Sy5rdysLn++2WE8VEBBB4gj3znVhvwcOaa8UTF2dyl2IbVnH1qVM+as6n0Ke+XsoMOObqITwY0z4PYD+JU98v5wwTvSSfD/Jesviv75FBy5F8DW8E/wBRJzzkw1qjfYP3lnSeV1PNgsQP8In93pfKcw2C1qnip+R+U8/aa/ci+r6s34LOjJdf0RuWDqXfyHwmz4RtJpuBqdM+XwE2nB1NJ0wc8jjiI5lnmhjMIeGab5TMtjy5kaoZkdpHqNM05HSKI9Yytxp6PlLCsZWYw6Tz60jTTKzjTH+Kv3p0qc4w63q0B21k++J0ebNlL4Zdq9DnjNYiavWTPVqn69bKPBVSn8Q02OtVCKWO5QSfIXlPs3DnMl94u7faYlj/ABMZ2x0ekUafN+/qcaL3byL2IkRatQuQaYFPg2YX8ctt3nNsppNX49T8+Xa8jgS4iJYCIiAIiIAiIgCIiAIiIBW7Qob+xhv7COPwMlYOtnQNx3EdhGhmStTzAiVmGq825B0Vjr3NuB+XumSTVGpd6S9dffadV8cewmbUo85Rqp9ak6+9SJxvZL2dT/eonb5xfaGH5jE1U3ZKrW8M119LTJtWGUZdqNuz5ZSiXeDqflD5fATacFV0mm0ns47wJseBrTDhqlsi9eNy+SpPReQqdSZS82OqZN09u0wO0+M8wu05yncuoniqZX4s6SXVaQMY2ky1Wd4I87Fp5sVRHYS3uUn5Cb/NO5IUL1nfgq282P8AJTNtqVAoLHQCers2O7Rcnxbfhl9DLi3epbkkQtpvfKnabnwU39Tb1mfBU7AnifgJBwwNVyx4+gG4f32y3Anah+5J1PD3716jlL4Vu+J9iIms5CIiAJD2pznM1OaF6vNtlF8vStprwkyIZKdmmal+Ldq/+XS/cH+2JtsTn0S5vxf3Nn66f9sP+uH/AJETFWUspAJUkbxa479Z8oIVUAsWPFiACfIACdDFwIi7PtiDW5yprTyc3f8AJjUHMBwOksZFx2MSiuZzYbtxJJO4Ko1Y9wmLA7Q506UqqjtdMo9Tf0lt1tXEql2k3nYnxESoEr9pYXMMwGvEdolhE51acakXGRMZOLuir2Zjb9BjrwPb3Hvmle0PAZMQtYDo1VsftJp93L7jNw2ns89env3kD4j+UqtpEYyg1J9Ko6VNu1huHmLjznk1ZS3Hh62v9L4O3Xz9vm91CymqsdOPV717jTKT3Cns0l9gK01ijdSVP9mWmCr2nkb9pXPQqQujaqNWSM8qMPXEmc+J16YxuBnd5hd5hqYkCQcRjpfpETGm2S6ta0rqtbMZDq4u8nbJwvOMATZd7HsUbzIV5uyO+4oK7Nr5MYbm6GY6FyW17Nw8rC/nPmLxJqsFXqg6DtPb/f8A1gxWPNUilSBy7gBxt8BLXZ2BFIXOrH07hPUjLp0qFL5I5OXPqXvy18+ScHvz1ei5GbCUMi248ZIiJ6kYqKsjK3d3Er9sV6lOkzUlu+gHaL6AgW1NyNJYRLp2dyk4uUWk7dfIibPrO9NWqLlc3uuotqRxkueHYAEncBefVYEXGoMhu7EU0ld36z1ERBYREQBESprbQroxH4K7rwZKlI38QxW0lRb09UvUrKSjr6N+gxWIK4ugh6j0q1vtKaRv7rjzMsKdTNm0Is1teOgNx3a28jMFPD53Wq62dVZVF75Q5Utr2nKvhbvk2JO9rcPuxFNX98EIiJBYREQBKvH7KWocy9F/Q+I+cy7T2rQwy5q1VKY4ZiAT9kb2PhNN2r7VMIlxRR6rcDoinwJ19JnxCoyju1bW95mzCYXE1XehFvr4eLyMfKDYzKc5WxO/x+sDx75SZSsjbR9qWJqgqlKkgPAqXP8AEbek1ityixDfTI7gAPlPDr4WDfwSy60fS4XZeK3bVN1d9/T7m8UcXaSPw+c2batc/pG8mI+E+fjKv+sf95/5zP8Ao/5eX5Nn+zN6yXmdFrY6V9XF3mknaNb9Y/7zfzgbUrj9I/vc/Ey6wv8ALy/JK2Q1pJG/YCkXPdNx2bsp2Ww0U72O7yHGcYococVT6tUi3gfiJd4T2lbQp73WoOxkU+q2M00qFNP9xu3UYMTsjFPOEo+f2Oz7Mw608wCEEG2drXbw7pYzlOzva4bgYjDi3FqbW/ga/wB6bvsHlZhMbpSqjP8AUfov5A9byvPYoSpqKhF+iPn8Xs/FUbzqwdueq8UX8RE0GAREQBIX4GMuUMwW/VBtp2A7wO68mxKShGXzL39nxWjJTa0IVWjlT8mApXUADfbeO++6TAZ9iFBRd14cA3cRES5AiJ4N7i27j7oB7iIgCJFxag2BvY31HCanyx2BjMVR/IYlxYEGlfItQXNukLG9raNceEzuu99xUb5c/K3o1e+ejO1GnGc1GclFPi728i+2jylweHvzlempG9Qczj/Itz6TTeUntOpBCuDzNWJsHZQFUa3ZVOrN2Ai3wPKsZhalJylRXV1NirAgjyknYOOXD4mjWdcy03QldLkK19L6XG8d4mWWLnJ20PrqX+n8PTh0jbqNZpZJS49evaSMTgsbiamasKhcq5LVMwsE1ZmLfRA4d4A3iScHyTZ7flaQD0y9Ns/QqEMqMlzbKwZgLMBraZMLtKjhHqGm7YhXDIVIK3pVFIcOxF0qXFOxW40Ou4SN/wDoSqLSWmOaUVQFqMWJNUKC5YZdbKthbTfrOLUVqb97FSVqasuHw258JZrO0baWva2VprcmRTpvUfnPySUy9MhabZ3qtTsCcwVRkJvre43XllsnkvRqM4emy2fCgA1KZPN1lfNkdRZmuLp25gLazWX5Q4kkE1STzXNa2a9O5IVwQQ6gk9a8xVNs4hrg1WtZQQGZVsmiDKLCy8BwhSgnp6FpYfFzi06iTfFOWWmiVlzffbhcvNj8nVqpXVgRXGfmQTZ70bNUQpvLFS3mhks4fDZMI5w6Klesyub1LKorqNCX+pmU/avNVq7TrsQWqVGIvYliSM2+xJ4zC1ZyLE3HYSSJXfSWS93v9/aOzwlacrynle+V8smrcMvlfav5M3nB7Co/hZovh1AbG5FB5wE01L51HS1A6Bzb9d+spOTuxFr1158ZKJYW1y61HK07ZzferniSEaUIr1Lg3bQWB6VwOwHsmT8YVv1lTrBusesNzb947ZO/G+hV4TEKLSnqkr55a5rN65eFy+pbLw60ar1qdQNRr0qZyuoJLtVz6OpA6gsJ82jyZpqnOB2WmlDDu9wrNzldM6oF6IsBckk8JR1dq1mDK1ViHcMwZiwLDc7X6zd8kfj+qc/OEVEdVDKVAQilonVsQQNAQQbabo3oW09+7FJUcQnvKXHNXby+HJKWX92ul1w0lYnkpVVXdGR1XD06w3qTSqAkME1sRZri+mUkXleNnYikc6qboEbMhLAXGdGzLfKbWPA21lnQ5V1FdKhUM6uh7ENMIaa0strBMrEafXO8mSa/KGiaFZekucs9JLWalUZsgy1BvpGjlRgdbrp2y9oPQ4Ori4ZTinp9npw1d8tbZJJPa9g+1SmKapiadQ1FUBqilTmI0zFTaxO+bdsrlpgcTYJXVWP0anQN+wE6E+BM/Paiwm88jPZ9UxWWrWLUsOTcW0qOP2Qdy/tHyBvedaWIqylZZmHaGyNn0afSSk4dmd3ySeb7mrc0dsOo38N880lIUAm5AAJ7e+R9m4Cnh6a0qS5aaCwGp95OpPfJk9E+PaV8vfr6iIiCCBgTWz1hUHQDg026OqlFuLDW4bNv7ZPiJLdyErIRESCRERAEREARMVUsB0Rc6aE2466zLIvwByn2z4unejSCrzpBYvYZgvSVVzfVJzG37E5c40Mv+W20jisdVYAsOc5tANeinQFvGxb/ADTxgOSuNrG1Og19N/Q38ekQbd88ivPeq367LrP0LZypYTBQhVkldXd3bXPjyvoa3+EKN88nF906Rh/ZRi3/ADj0EHeS59wS3rLCn7HL9bEqPCgD6l50VCb/AKThU2thY5Ksv+L+zOTfhJuPGd42L7PMAaNNqtIvUakha9RwMxUFrBSON5TYf2OUQ6l8SzIOsq0whbuzZjb3TqCKAABuAsJqo0EruSPD2ltaVTdVCpLjeza5W5dZQU+RGz13YZPMu3xaa7yp2PhUzUqeGRGyjK4pu2/jdFa3EWNt3fOiSi5SqCBpup1T7gs546KjQbS5ep5tDE1nVV5yfeyh5F8ncNUw9quHpuyOy84VN3+lqGAItmt5S6qcitntvwyeWdfgZZ7Ga9CkR+rX4SdO9GnHo43zyXArVxVbpJNSazfFnGPazyZw+CpUqmHp83mdlfpMwPRuvXY23NunMhiTP03yn5PUdoUeZrZsoYMrKQGVgCARcEbmI1HGapS9keCA/OVr93ND/wCDOVXDtyvFI9jA7YjClu1pyvfXX1ZxEYgzJSuZ2mr7J8N9GtVHiEb4ASrxXskqD83iUbuZGp+oLfCcJUKi0j5nq0tr4KT+Ko++LX3OZH5z9C8h9tfhuDp1T1x0H+2oBv5ghv8ANOUbS9nW0aOq00qjtRgf4SAx90uPZcK1KtWwtRq2HqPTDqCovmU2NlqKQTZr7voycMp052a1OW2pYfGYfpKU03HPueTy169OB2CQNrV3p0nenTNSoFuFGtz4bz5azFQ2aaQJWrVqVMp1q1Cyk20ugso1+qBJOzcYK1KnVAIWpTVwDvAYA2PfrPQu2raM+RsoveXxJNdV+NudnZ8fA+4Koz01Z1yOVBZb3ykjVb93bJURLHJ6iIiAIiIAiIgCIiAJjqJcEdotpode+ZIgFLsXY9DDNUWjRRMuVQQOkeiGJLnVj0uJ4S6nkAa9+/4fKYq9dUsWvqbaBm9ADK047qt2+buTObb3pPvZniIliBET5FgfZQ7afM7r9XDN73P/AB9Zc1KyrvP8/dNNWrVLV6joVNQ6A2uFAso0O+wE8/aNVQgo836Z+tjThoXlcvOSGIz4Wn2rdT5HT0Il3NJ5GYlqRanUUqjG4PAEdvj8hNyp1VYXUgjtGol9n1lUoR5pWfcMZT3K0uV7+OZkiIm0zCIiAJjNMEgkAkbjxGltOzQzJEASJs7BrQpU6SXyogVb6myiwuZLiCbu1hERBAiIgCIiAIiIAiIgCIiAIiIAmN6oEyTFVoBoBR7WY1WAWrUQAfo2y38ZHrYbJTJ5yq1vrPf1tLSrsnW6sQf77Zgr7LqspXOLeAmCthHUm3LNcM2aIVlFJGlYnli1B8gpZr8S4/2SVhtvmt9C3nf5SRjOQPONmLtfut/KSMLyPen1W94mGrs+u47sLW7TX+ow9rpO/eZsJhi30rX7r/OSTyXU9Ln6ov8AVssyUNmYhNxT90/7pOWlibWLIB3KfmZ2wmz92P7qTfb9jPPEtu8WVSbPNHq1658ahPpaXGzcXdQGN211O/ebTF+LajdZ/cBJGG2aE4zZRw8qc97hbTM5VKqmrE8G8+z4q2n2aziIiIAiIgCIiAIiIAiIgH//2Q==",
            height: 250,
            width: 400,),
            const Text("Congratulations",
            style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.w500,
            ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Text("You have completed the quiz",
            style: TextStyle(
              fontSize: 23,
            ),
            ),
            const SizedBox(height: 15,),
            Text("$noofCorrectAnswer/${allQuestions.length}"),
            ElevatedButton(onPressed: (){
              questionIndex=0;
              questionScreen=true;
              noofCorrectAnswer=0;
              selectedAnswerIndex=-1;
              setState(() {
                
              });

            },
             child: const Text("Reset",
             style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.normal  ),
              ),
              ),
          ],
        ),
      );
    }
  }
  @override
  Widget build(BuildContext context){
    return isQuestionScreen();
  }
}
