import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super. key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: IndiaFlag(title: "India Flag"),
    );
  }
}

class IndiaFlag extends StatefulWidget {
  const IndiaFlag({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<IndiaFlag> createState() => _IndiaFlagState();
}

class _IndiaFlagState extends State<IndiaFlag> {
  int _counter = -1;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        child: const Text("add"),
      ),
      body: Center(
        child: Container(
          color: Colors.grey,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (_counter >= 0)
                Container(
                  height: 500,
                  width: 20,
                  color: Colors.black,
                ),
              Column(
                children: [
                  if (_counter >= 1)
                    Container(
                      height: 80,
                      width: 250,
                      color: Colors.orange,
                    ),
                  if (_counter >= 2)
                    Container(
                      height: 80,
                      width: 250,
                      color: Colors.white,
                      child: _counter >= 3
                          ? Image.network(
                              "https://t3.ftcdn.net/jpg/03/11/13/46/360_F_311134651_RXMvbUB3h089Js0ODvuHrttmsON9Tpik.jpg",
                            )
                          : Container(),
                    ),
                  if (_counter >= 4)
                    Container(
                      height: 80,
                      width: 250,
                      color: Colors.green,
                    ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}