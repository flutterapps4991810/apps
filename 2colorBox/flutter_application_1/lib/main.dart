import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: ToogleBox(),
      debugShowCheckedModeBanner: false,
        
    );
  }
}
class ToogleBox extends StatefulWidget{
  const ToogleBox({super.key});

  @override
  State<ToogleBox> createState(){
    return _ToogleBoxState();
  }
}
class _ToogleBoxState extends State<ToogleBox>{
  bool box1Color=false;
  bool box2Color=false;

  Color setBox1Color(){
    if(box1Color==false){
      return Colors.red;
    }else{
      return Colors.black;
    }
  }

  Color setBox2Color(){
    if(box2Color==false){
      return Colors.black;
    }else{
      return Colors.red;
    }
  }

  int counter=0;
  
  @override
  Widget build(BuildContext context){
    return Scaffold(
    appBar: AppBar(
      title: const Text("Toogle Box"),
      centerTitle: true,
      backgroundColor: Colors.blue,
    ),
    body: Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        
        children: [
          Column(
            
            children: [
              Container(
                height: 100,
                width: 100,
                color: setBox1Color(),
              ),

              const SizedBox(
                height: 20,
              ),

              ElevatedButton(
                onPressed: () {
                  setState(() {
                    if(box1Color==false){
                      box1Color=true;
                    }else{
                      box1Color=false;
                    }
                  });
                } ,
                child: const Text("Button1"),
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                height: 100,
                width: 100,
                color: setBox2Color(),
              ),

              const SizedBox(
                height: 20,
              ),

              ElevatedButton(
                onPressed: () {
                  setState(() {
                    if(box2Color==false){
                      box2Color=true;
                    }else{
                      box2Color=false;
                    }
                  });
                } ,
                child: const Text("Button 2"),
              ),
            ],
          ),
        ]
      ),
    )
    );
  }
}
