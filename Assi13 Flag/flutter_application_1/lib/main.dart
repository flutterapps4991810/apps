import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Indian Flag"),
        ),
        body: Row(
          mainAxisAlignment: MainAxisAlignment.center,
         
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
           
            children: [
              const SizedBox(
                height: 20,
              ),
            Container(
              height: 560,
              width: 10,
              color: Colors.brown,
            ),
            ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [

            Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                const SizedBox(
                height: 60,
              ),
             Container(
              height: 50,
              width: 200,
              color: Colors.orange,
             ),
            
          
             Container(
              height: 50,
              width: 200,
              color: Colors.white,
            
              child: Center(
                child:Image.network("https://tse4.mm.bing.net/th?id=OIP.hS_CC3k7DV3O5WsSoP3I0QHaHa&pid=Api&P=0&h=180"),
              )
            ),
            
             Container(
              height: 50,
              width: 200,
              color: Colors.green,
             ),

             const SizedBox(
              height: 380,
             ),

             Container(
              child: const Text("Happy Republic Day",style: TextStyle(
                color: Colors.blue,
                fontSize: 20.0,
                fontStyle: FontStyle.italic,
              ),),
              
             ),
          ],
          ),
        ],
        ),
        
        ]

        ),
      ),
    );
              
  }
}
