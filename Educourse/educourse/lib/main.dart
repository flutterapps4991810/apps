import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: UiDesignApp(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class UiDesignApp extends StatefulWidget {
  @override
  State createState() => _UiDesignApp();
}

class _UiDesignApp extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(205, 218, 218, 1),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 47, left: 20, right: 20),
            child: Row(
              children: [
                IconButton(
                  onPressed: () {},
                  icon: const Icon(Icons.menu, size: 30),
                ),
                const Spacer(),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(Icons.notifications_active_outlined, size: 30),
                ),
              ],
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(top: 45, left: 20),
            child: Column(
              children: [
                Text(
                "Welcome to New",
               
                )],

            ),
          ),
        ],
      ),
    );
  }
}