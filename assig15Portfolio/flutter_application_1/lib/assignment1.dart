import 'package:flutter/material.dart';

class Assignment1 extends StatefulWidget{
  const Assignment1({super.key});

  @override
  State<Assignment1> createState() =>_Assignment1State();

}
class _Assignment1State extends State<Assignment1>{
  int counter=-1;

  @override
  Widget build(BuildContext context){
    
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: const Text("Portfolio"),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          setState(() {
            counter++;
          });
        },
        child: const Text("Add"),
         ),
         body: Container(
          height: double.infinity,
          width: double.infinity,
          color: Colors.grey,
          child: Column(
            
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            
            children: [
              (counter>=0)
              ? Container(
                height: 200,
                width: 200,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Name:- Prachi Gole"),
                    Image.asset('c:\Users\golep\Downloads\Prachi photo.jpg',),
                  ]),
              )
              :Container(),
              (counter>=1)
              ? Container(
                height: 200,
                width: 200,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("College:- TSSM BSCOER"),
                    Image.network('https://cache.careers360.mobi/media/presets/720X480/colleges/social-media/media-gallery/1957/2020/9/5/Campus%20View%20of%20Bhivarabai%20Sawant%20College%20of%20Engineering%20and%20Research%20Pune_Campus-View.jpg',),
                  ]),
              )
              :Container(),
              (counter>=2)
              ? Container(
                height: 200,
                width: 200,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Dream Company:- Google"),
                    Image.asset('https://assets.entrepreneur.com/content/3x2/1300/20150805204041-google-company-building-corporate.jpeg',),
                  ]),
              )
              :Container(),
            ],
          ),
         ),
    );
  }
}