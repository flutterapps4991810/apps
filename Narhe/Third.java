import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.control.Button;

public class Third extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        // Load the background image
        Image backgroundImage = new Image("Second.jpg"); // Change "background.jpg" to the actual image file name
        ImageView backgroundView = new ImageView(backgroundImage);

        // Create buttons
        Button mainButton = new Button("Places");
        Button button1 = new Button("Core2Web");
        Button button2 = new Button("Swaminarayan Temple");
        Button button3 = new Button("Nawale Bridge");
        Button button4 = new Button("Katraj Lake");
        Button button5 = new Button("Sunset Point");

        // Create a layout for the buttons
        VBox buttonLayout = new VBox(20); // 10 is the spacing between buttons
        buttonLayout.getChildren().addAll(mainButton, button1, button2, button3, button4, button5);

        // Create a root layout to hold the background and buttons
        StackPane root = new StackPane();
        root.getChildren().addAll(backgroundView, buttonLayout);

        // Create a scene and set it on the stage
        Scene scene = new Scene(root, 500, 300); // Set the width and height as needed
        primaryStage.setScene(scene);

        primaryStage.setTitle("Background with Buttons");
        primaryStage.show();
    }
}
