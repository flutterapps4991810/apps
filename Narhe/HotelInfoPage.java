import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;

public class HotelInfoPage extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Hotels Information Page");

        // Create a StackPane for the background image
        StackPane root = new StackPane();
        Image backgroundImage = new Image("taj2.jpeg"); // Change to the actual image file path
        ImageView backgroundView = new ImageView(backgroundImage);
        root.getChildren().add(backgroundView);

        // Create a VBox to hold the buttons
        VBox buttonContainer = new VBox(20); // 20 pixels spacing between buttons
        buttonContainer.setStyle("-fx-alignment: center;");

        // Create buttons for each hotel
        Button hotel1Button = createHotelButton("Aashirwad Hotel", "It's located near Navale Bridge and serves Alcohol also. Staff her is very supportive and friendly and specially the owner Mr. PRAVEEN is very helpful in giving suggestions if you are confused.\r\n" + //
                "Must try dishes on their menu are Stuff Tomato, Tava Besan , Cream of Chicken / Veg soup , Butter Chicken Chicken Roast Handi , American Choupsey and Fried Rice. The list goes on , depending upon your choice of cuisine you can select.\r\n" + //
                "Overall a nice dining experience which is not too expensive .... Guys Ashirwad is giving some special offers for India matches , but only concern is dinning offers");
        Button hotel2Button = createHotelButton("Thorat Barbique", "Ultimate Misal and Pavbhaji with 4 different varieties that too unlimited.. What !!! Yes you can't best deal than this Either you can choose from Smokey misal pav with 4 varieties or unlimited pav bhaji with 4 Varities\r\n" + //
                "\r\n" + //
                " Best taste\r\n" + //
                " Best Varity\r\n" + //
                " Best price\r\n" + //
                " Unlimited portion\r\n" + //
                " Good service and ambience");
        Button hotel3Button = createHotelButton("Shriji Hotel", " At (RN), we’re serving up more than (TF). In fact, (RN) Famous (recipe) is one of our unexpected specialties. Reminiscent of butcher shops back in the day, each slow-smoked, sizzling prime chop measures seven-fingers high. Our signature recipe, that we have perfected for more than four decades, is rubbed with a secret blend of seasonings, cured and roasted on a rotisserie with pecan wood for up to six hours before it’s topped with (RN) signature herb-garlic butter, then carved tableside.\r\n" + //
                "\r\n" + //
                "03. (RN) specializes in delicious food featuring fresh ingredients and masterful preparation by the (RN) culinary team. Whether you’re ordering a multi-course meal or grabbing a drink and pizza at the bar, (RN’s) lively, casual yet upscale atmosphere makes it perfect");
        Button hotel4Button = createHotelButton("Hotel Kolhapuri", "Every dish is a in kolhapuri special dish…There is a special taste to the cuisine at kolhapuri. Crafted by specially blended masalas – you will always enjoy the taste herePlus it offers diverse menus – other than the superb South Indian..");
        Button hotel5Button = createHotelButton("Oh Pune Hotel", "Calling all foodies in search of bliss! \r\n" + //
                "This monsoon month brings forth the perfect opportunity to revel in the mouthwatering delight of aromatic biriyani. It's an experience you won't want to miss! and you should not miss it too 😉\r\n" + //
                "Walhekar Properties, Near HDFC Bank, Mumbai\r\n" + //
                "Bangalore Narhe, Pune\r\n" + //
                "🕑12:00 PM to 12:00 AM\r\n" + //
                "For More Contact Us on: 7030731818");

        // Add buttons to the button container
        buttonContainer.getChildren().addAll(hotel1Button, hotel2Button, hotel3Button, hotel4Button, hotel5Button);

        // Add the button container to the StackPane (on top of the background image)
        root.getChildren().add(buttonContainer);

        // Create a scene and set it on the stage
        Scene scene = new Scene(root, 400, 300); // Set the width and height as needed
        primaryStage.setScene(scene);

        primaryStage.show();
    }

    // Utility method to create hotel buttons
    private Button createHotelButton(String hotelName, String hotelInfo) {
        Button button = new Button(hotelName);
        button.setStyle("-fx-font-size: 20px;"); // Large size for the button text
        button.setOnAction(e -> displayHotelInfo(hotelName, hotelInfo));
        return button;
    }

    // Method to display hotel information in a TextArea
    private void displayHotelInfo(String hotelName, String hotelInfo) {
        Stage infoStage = new Stage();
        infoStage.setTitle(hotelName + " Information");

        TextArea infoTextArea = new TextArea(hotelInfo);
        infoTextArea.setEditable(false);
        infoTextArea.setWrapText(true);

        StackPane infoLayout = new StackPane(infoTextArea);
        Scene infoScene = new Scene(infoLayout, 400, 300);
        infoStage.setScene(infoScene);
        infoStage.show();
    }
}
