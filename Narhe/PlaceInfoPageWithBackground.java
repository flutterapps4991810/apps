import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BackgroundPosition;

public class PlaceInfoPageWithBackground extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Places Information Page");

        // Create a StackPane to hold the background image and buttons
        StackPane root = new StackPane();

        // Set the background image (replace with your image file)
        BackgroundImage backgroundImage = new BackgroundImage(
                new Image("core.jpg", 800, 600, false, true),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER,
                new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, true, true)
        );

        root.setBackground(new Background(backgroundImage));

        // Create a VBox to hold the buttons
        VBox buttonContainer = new VBox(20); // 20 pixels spacing between buttons
        buttonContainer.setStyle("-fx-alignment: center;");

        // Create buttons for each place
        Button place1Button = createPlaceButton("Core2Web", "Core2Web Technologies is a well-known training institute located in Pune, India. They offer a wide range of courses and training programs in various IT and programming fields. Some of the key features and information about Core2Web Technologies in Pune include:\r\n" + //
                "\r\n" + //
                "Training Programs: Core2Web Technologies offers training programs in diverse areas, including web development, mobile app development, software testing, digital marketing, and more.\r\n" + //
                "\r\n" + //
                "Experienced Instructors: The institute employs experienced and knowledgeable instructors who provide practical insights and guidance to students.\r\n" + //
                "\r\n" + //
                "Hands-on Learning: Courses at Core2Web Technologies often include hands-on training and real-world projects, allowing students to gain practical experience.\r\n" + //
                "\r\n" + //
                "Certification: Upon successful completion of courses, students receive certifications that are recognized in the industry, which can enhance their job prospects.\r\n" + //
                "\r\n" + //
                "Placement Assistance: The institute provides placement assistance to help students find job opportunities after completing their training.\r\n" + //
                "\r\n" + //
                "Flexible Learning Options: Core2Web Technologies offers flexible learning options, including classroom-based training, online courses, and weekend classes, to accommodate different schedules.\r\n" + //
                "\r\n" + //
                "Industry-Relevant Curriculum: The institute's curriculum is designed to be up-to-date with industry standards and technologies.\r\n" + //
                "\r\n" + //
                "Positive Reputation: Core2Web Technologies has built a positive reputation in the field of IT education and training in Pune.\r\n" + //
                "\r\n" + //
                
                "");
        Button place2Button = createPlaceButton("SwamiNarayan Temple", "The Swaminarayan Temple in Pune is a prominent Hindu temple dedicated to Lord Swaminarayan, a revered deity in the Swaminarayan Sampradaya. Here's some brief information about the Swaminarayan Temple in Pune:\r\n" + //
                "\r\n" + //
                "Location: The Swaminarayan Temple is located in the heart of Pune city, in the Sadashiv Peth area, making it easily accessible for both locals and visitors.\r\n" + //
                "\r\n" + //
                "Architectural Beauty: The temple is known for its stunning architecture and intricate carvings, showcasing traditional Indian craftsmanship. It features a beautiful marble facade and an ornate interior.\r\n" + //
                "\r\n" + //
                "Spiritual Significance: The temple is a place of worship and spiritual significance for followers of the Swaminarayan sect. Devotees come here to seek blessings, offer prayers, and participate in religious ceremonies and festivals.\r\n" + //
                "\r\n" + //
                "Religious Activities: The temple hosts regular religious and cultural events, including bhajans (devotional songs), discourses, and festivals celebrating the life and teachings of Lord Swaminarayan.\r\n" + //
                "\r\n" + //
                "Community Services: In addition to its religious activities, the Swaminarayan Temple in Pune is also involved in various charitable and community service initiatives, including educational programs and food distribution to the needy.\r\n" + //
                "\r\n" + //
                "Open to All: The temple is open to people of all faiths and backgrounds who wish to visit and explore the spiritual and cultural aspects of Hinduism.\r\n" + //
                "\r\n" + //
                "Peaceful Atmosphere: The temple provides a serene and peaceful environment for meditation and reflection, offering a respite from the bustling city life.");
        Button place3Button = createPlaceButton("Sunset Point", "Sunset Point in Narhe, Pune is a popular scenic spot where people gather to witness the breathtaking views of the sun setting below the horizon. Here is some brief information about Sunset Point in Narhe:\r\n" + //
                "\r\n" + //
                "Location: Sunset Point is located in Narhe, a suburb of Pune, Maharashtra, India. It offers an excellent vantage point to enjoy the evening sky.\r\n" + //
                "\r\n" + //
                "Scenic Beauty: This location is known for its picturesque views, especially during the twilight hours when the sun sets. The panoramic view of the city and the surrounding landscape adds to its charm.\r\n" + //
                "\r\n" + //
                "Relaxation and Recreation: Sunset Point serves as a serene and peaceful place for residents and visitors to relax, unwind, and enjoy the natural beauty of the area.\r\n" + //
                "\r\n" + //
                "Local Attraction: It's a favorite spot for locals who often gather here in the evenings to spend quality time with family and friends, and to capture stunning photographs of the setting sun.\r\n" + //
                "\r\n" + //
                "Nature Enthusiasts: Nature enthusiasts and photographers also find this location appealing for its scenic photography opportunities.\r\n" + //
                "\r\n" + //
                "Accessibility: Sunset Point is easily accessible by road and is a short drive from various parts of Pune, making it a convenient destination for those looking for a quick escape from the city.");
        Button place4Button = createPlaceButton("I Love Narhe", "\"I Love Narhe Point\" is a popular local landmark and a gathering spot located in the Narhe area of Pune, India. While there isn't an extensive amount of information available, it is commonly known as a meeting place, often used for social gatherings and community events. It has likely become an affectionate spot for residents of Narhe to come together and enjoy their surroundings. The name itself suggests a strong sense of community and attachment to the locality. Specific details, such as the history or features of the place, may vary and can be obtained from local residents or authorities in Narhe.");
        Button place5Button = createPlaceButton("Katraj Lake", "Katraj Lake is a picturesque man-made reservoir located in the Narhe suburb of Pune, Maharashtra, India. Here's some brief information about Katraj Lake:\r\n" + //
                "\r\n" + //
                "Location: Katraj Lake is situated in the Katraj-Narhe area, a part of the city of Pune.\r\n" + //
                "\r\n" + //
                "Scenic Beauty: The lake is known for its natural beauty and serene surroundings. It offers a tranquil escape from the hustle and bustle of the city.\r\n" + //
                "\r\n" + //
                "Recreational Area: The lake serves as a popular recreational spot for locals and visitors. It's an ideal place for a leisurely walk, picnics, and spending quality time in a peaceful environment.\r\n" + //
                "\r\n" + //
                "Boating: Boating facilities are available at Katraj Lake, allowing visitors to enjoy a boat ride on the calm waters.\r\n" + //
                "\r\n" + //
                "Bird Watching: The lake is also a habitat for various bird species, making it a favorite spot for birdwatchers and nature enthusiasts.\r\n" + //
                "\r\n" + //
                "Greenery: The area around the lake is dotted with lush greenery, making it a pleasant place for nature lovers.\r\n" + //
                "\r\n" + //
                "Walking and Jogging: The well-maintained walking paths around the lake are perfect for those who enjoy walking or jogging in a scenic setting.\r\n" + //
                "\r\n" + //
                "Local Attraction: Katraj Lake is often visited by residents of Katraj and Narhe, and it's a place where families and friends gather to relax and unwind.");

        // Add buttons to the button container
        buttonContainer.getChildren().addAll(place1Button, place2Button, place3Button, place4Button, place5Button);

        // Add the button container to the StackPane
        root.getChildren().add(buttonContainer);

        // Create a scene and set it on the stage
        Scene scene = new Scene(root, 800, 600); // Set the width and height as needed
        primaryStage.setScene(scene);

        primaryStage.show();
    }

    // Utility method to create place buttons
    private Button createPlaceButton(String placeName, String placeInfo) {
        Button button = new Button(placeName);
        button.setStyle("-fx-font-size: 20px;"); // Large size for the button text
        button.setOnAction(e -> displayPlaceInfo(placeName, placeInfo));
        return button;
    }

    // Method to display place information in a TextArea
    private void displayPlaceInfo(String placeName, String placeInfo) {
        Stage infoStage = new Stage();
        infoStage.setTitle(placeName + " Information");

        TextArea infoTextArea = new TextArea(placeInfo);
        infoTextArea.setEditable(false);
        infoTextArea.setWrapText(true);

        StackPane infoLayout = new StackPane(infoTextArea);
        Scene infoScene = new Scene(infoLayout, 400, 300);
        infoStage.setScene(infoScene);
        infoStage.show();
    }
}
