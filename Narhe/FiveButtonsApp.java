import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class FiveButtonsApp extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        // Load the background image
        Image backgroundImage = new Image("Second.jpg"); // Replace with the path to your image

        // Create an ImageView to display the background image
        ImageView backgroundImageView = new ImageView(backgroundImage);

        // Create buttons
        Button button1 = new Button("Places");
        Button button2 = new Button("Colleges");
        Button button3 = new Button("Hotels");
        Button button4 = new Button("Cafe");
        Button button5 = new Button("Climate");

        // Create a vertical layout for the buttons
        VBox buttonContainer = new VBox(30); // 10 is the spacing between buttons
        buttonContainer.getChildren().addAll(button1, button2, button3, button4, button5);

        // Create a layout to stack the background image and button container
        StackPane root = new StackPane();
        root.getChildren().addAll(backgroundImageView, buttonContainer);

        // Create a scene
        Scene scene = new Scene(root, backgroundImage.getWidth(), backgroundImage.getHeight());

        // Set the scene for the stage
        primaryStage.setScene(scene);

        // Set the title of the window
        primaryStage.setTitle("Background Image with Buttons");

        // Show the window
        primaryStage.show();

        // Button actions (e.g., print a message when clicked)
        button1.setOnAction(e -> System.out.println("Button 1 clicked"));
        button2.setOnAction(e -> System.out.println("Button 2 clicked"));
        button3.setOnAction(e -> System.out.println("Button 3 clicked"));
        button4.setOnAction(e -> System.out.println("Button 4 clicked"));
        button5.setOnAction(e -> System.out.println("Button 5 clicked"));
    }
}
