import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.control.TextArea;

public class CollegeInfoPage extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("College Information Page");

        // Load the background image
        Image backgroundImage = new Image("clg.jpg"); // Replace with your background image file
        ImageView backgroundView = new ImageView(backgroundImage);

        // Create a VBox to hold the buttons and set the alignment to the center
        VBox buttonContainer = new VBox(20); // 20 pixels spacing between buttons
        buttonContainer.setStyle("-fx-alignment: center;");

        // Create buttons for each college
        Button college1Button = createCollegeButton("TSSM BSCOER COLLEGE", "TSSM BSCOER (TSSM Bhivarabai Sawant College of Engineering and Research) is an engineering college located in Narhe, Pune, India. Here is some brief information about the college:\r\n" + //
                "\r\n" + //
                "Location: TSSM BSCOER is situated in Narhe, which is a locality in Pune, Maharashtra, known for its proximity to the city and access to educational institutions.\r\n" + //
                "\r\n" + //
                "Affiliation: The college is affiliated with Savitribai Phule Pune University (formerly known as the University of Pune).\r\n" + //
                "\r\n" + //
                "Courses: TSSM BSCOER offers undergraduate and postgraduate programs in engineering and technology fields. It typically offers programs in branches such as Computer Engineering, Electronics and Telecommunication Engineering, Information Technology, and more.\r\n" + //
                "\r\n" + //
                "Infrastructure: The college campus is equipped with modern infrastructure and facilities to support academic and co-curricular activities. This includes well-equipped laboratories, a library, and sports facilities.\r\n" + //
                "\r\n" + //
                "Faculty: TSSM BSCOER has a team of experienced and qualified faculty members who are dedicated to providing quality education to the students.\r\n" + //
                "\r\n" + //
                "Student Activities: The college often promotes extracurricular activities, including student clubs and events, to encourage the overall development of its students.\r\n" + //
                "\r\n" + //
                "Placement Opportunities: TSSM BSCOER typically has a placement cell that assists students in securing job opportunities with various companies and organizations.\r\n" + //
                "\r\n" + //
                "");
        Button college2Button = createCollegeButton("JSPM NTC COLLEGE", "JSPM (Jayawant Shikshan Prasarak Mandal) College located in Narhe, Pune, is a prominent educational institution in the Pune region. Please note that the specific information about the college may have changed since then, and I recommend visiting the college's official website or contacting them directly for the most up-to-date information.\r\n" + //
                "\r\n" + //
                "Here's some general information about JSPM College, Narhe, Pune:\r\n" + //
                "\r\n" + //
                "Location: JSPM College is situated in Narhe, a suburban area in the southwestern part of Pune, Maharashtra, India.\r\n" + //
                "\r\n" + //
                "Courses: The college offers a range of undergraduate and postgraduate courses in various fields, including engineering, management, computer applications, pharmacy, and more.\r\n" + //
                "\r\n" + //
                "Affiliation: The college may be affiliated with Savitribai Phule Pune University (formerly Pune University) or other relevant affiliating bodies, depending on the course.\r\n" + //
                "\r\n" + //
                "Facilities: It typically provides students with modern infrastructure, well-equipped classrooms, laboratories, libraries, and other necessary facilities for academic and extracurricular activities.\r\n" + //
                "\r\n" + //
                "Faculty: The college is known for its qualified and experienced faculty members who guide students through their academic journey.\r\n" + //
                "\r\n" + //
                "Placement: JSPM College may have a placement cell that assists students in finding job opportunities and internships. They may have tie-ups with various companies and organizations for placement activities.\r\n" + //
                "\r\n" + //
                "Extracurricular Activities: The college may encourage students to participate in cultural events, sports, technical competitions, and other extracurricular activities to ensure their holistic development.");
        Button college3Button = createCollegeButton("SINHGAD COLLEGE", "Sinhgad College Narhe, Pune is part of the prestigious Sinhgad Technical Education Society, which is known for its educational excellence in the field of engineering, technology, and management. Here is some brief information about Sinhgad College Narhe, Pune:\r\n" + //
                "\r\n" + //
                "Location: Sinhgad College Narhe is located in the Narhe area of Pune, Maharashtra, India.\r\n" + //
                "\r\n" + //
                "Courses: The college offers a wide range of undergraduate and postgraduate programs in fields such as engineering, management, computer science, and more.\r\n" + //
                "\r\n" + //
                "Reputation: Sinhgad College Narhe is renowned for its quality education and is considered one of the leading institutions in Pune. It is known for its well-qualified faculty, state-of-the-art facilities, and a strong emphasis on practical learning.\r\n" + //
                "\r\n" + //
                "Infrastructure: The campus is equipped with modern infrastructure and facilities, including well-equipped laboratories, libraries, sports amenities, and a conducive learning environment.\r\n" + //
                "\r\n" + //
                "Placements: The college has a strong placement record, with many students securing job opportunities with reputed companies in India and abroad.\r\n" + //
                "\r\n" + //
                "Research and Innovation: Sinhgad College Narhe encourages research and innovation, and students often engage in various projects and research activities.");
        Button college4Button = createCollegeButton("ZEAL COLLEGE", "Zeal College, located in Narhe, Pune, is an educational institution that offers a range of undergraduate and postgraduate programs in various fields of study. It is known for its commitment to providing quality education and fostering academic excellence. The college is equipped with modern facilities, including well-equipped classrooms, libraries, laboratories, and experienced faculty. Zeal College aims to prepare students for the challenges of the professional world by imparting knowledge and skills, making it a notable educational institution in the Pune region. For more detailed and up-to-date information, I recommend visiting the official website of Zeal College or contacting the college directly.");
        Button college5Button = createCollegeButton("ABHINAV COLLEGE", "Abhinav Education Society is situated in Pune in Maharashtra state of India. Abhinav Education Society offers 12 courses across 5 streams namely Science, IT, Management, Engineering, Education..Popular degrees offered at Abhinav Education Society include B.Ed, B.Tech, MBA, MCA, M.Com..Besides a robust teaching pedagogy, Abhinav Education Society is also a leader in research and innovation.Focus is given to activities beyond academics at Abhinav Education Society, which is evident from its infrastructure, extracurricular activities and national & international collaborations. The placement at Abhinav Education Society is varied, with recruitment options both in corporates and public sector as well as entrepreneurship.");

        // Add buttons to the button container
        buttonContainer.getChildren().addAll(college1Button, college2Button, college3Button, college4Button, college5Button);

        // Create a StackPane to layer the background and button container
        StackPane root = new StackPane();
        root.getChildren().addAll(backgroundView, buttonContainer);

        // Create a scene and set it on the stage
        Scene scene = new Scene(root, 500, 400); // Set the width and height as needed
        primaryStage.setScene(scene);

        primaryStage.show();
    }

    // Utility method to create college buttons
    private Button createCollegeButton(String collegeName, String collegeInfo) {
        Button button = new Button(collegeName);
        button.setStyle("-fx-font-size: 20px;"); // Large size for the button text
        button.setOnAction(e -> displayCollegeInfo(collegeName, collegeInfo));
        return button;
    }

    // Method to display college information in a TextArea
    private void displayCollegeInfo(String collegeName, String collegeInfo) {
        Stage infoStage = new Stage();
        infoStage.setTitle(collegeName + " Information");

        TextArea infoTextArea = new TextArea(collegeInfo);
        infoTextArea.setEditable(false);
        infoTextArea.setWrapText(true);

        StackPane infoLayout = new StackPane(infoTextArea);
        Scene infoScene = new Scene(infoLayout, 400, 300);
        infoStage.setScene(infoScene);
        infoStage.show();
    }
}
