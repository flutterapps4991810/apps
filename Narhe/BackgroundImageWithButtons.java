import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class BackgroundImageWithButtons extends Application {

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("JavaFX Background Image with Buttons");

        // Load the background image
        Image backgroundImage = new Image("First.jpeg"); // You need to provide the path to your image

        // Create an ImageView to display the background image
        ImageView backgroundImageView = new ImageView(backgroundImage);

        // Create two buttons
        Button button1 = new Button("Button 1");
        Button button2 = new Button("IIT Narhe");

        // Set click actions for the buttons
        button1.setOnAction(event -> {
            System.out.println("Button 1 clicked!");
        });

        button2.setOnAction(event -> {
            System.out.println("Button 2 clicked!");
        });

        // Create a layout to stack the background image and buttons
        StackPane layout = new StackPane();
        layout.getChildren().add(backgroundImageView);
        layout.getChildren().addAll(button1, button2);

        // Set the layout as the root of the scene
        Scene scene = new Scene(layout, 500, 500); // Adjust the size as needed

        // Set the scene for the stage and show it
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
 
    

