import 'package:flutter/material.dart';

class assignment9 extends StatefulWidget{
   const assignment9({super.key});

  
  State<assignment9>createState()=> _assignment9state();
}
class _assignment9state extends State<assignment9>{

  
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: const Text(
          "Netflix",
          style: TextStyle(
            fontStyle: FontStyle.italic,
            color: Colors.black,
            fontSize: 30,
          ),
        ),
      ),
      body: ListView(
        children: [
         Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Movie"),
            SingleChildScrollView(scrollDirection: Axis.horizontal,
            child:Row(
              children: [
                
                Container(
                  color: Colors.white,
                  child: Image.network(
                    "https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC/et00311762-lmdexnggxy-portrait.jpg",

                    width: 150,
                    height: 220,
                  ),
                ),
                
                Container(
                  color: Colors.white,
                  child: Image.network(
                    "https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC/et00311762-lmdexnggxy-portrait.jpg",

                    width: 150,
                    height: 220,
                  ),
                ),
                
                Container(
                  color: Colors.white,
                  child: Image.network(
                    "https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC/et00311762-lmdexnggxy-portrait.jpg",

                    width: 150,
                    height: 220,
                  ),
                ),
                
              ],
            ),
            ),
            SizedBox(
              height: 20,
            ),
            
            Text("Web Series"),
            SingleChildScrollView( scrollDirection: Axis.horizontal,
            child:Row(
              children: [
                Container(
                  color: Colors.white,
                  child: Image.network(
                    "https://assetscdn1.paytm.com/images/catalog/product/H/HO/HOMSHERLOCK-HOLHK-P63024784A1CC1B/1563111214645_0..jpg",

                    width: 150,
                    height: 220,
                  ),
                ),
                Container(
                  color: Colors.white,
                  child: Image.network(
                    "https://dnm.nflximg.net/api/v6/2DuQlx0fM4wd1nzqm5BFBi6ILa8/AAAAQeIeKt7LlqIJPKrT4aQijclj7K43xRSU3dQXNESNdNbnnJbT6LLWVRT9srUUbHbOo-iOH-8v3o16pUDMQ6tCgNGlkvfwvDOprROIZpQ2rgHtop9rHvbYlvzavMmUSGBCXjynJ80dn4nqZzZmzIUJMQpS.jpg?r=943",

                    width: 150,
                    height: 220,
                  ),
                ),
                Container(
                  color: Colors.white,
                  child: Image.network(
                    "https://www.tallengestore.com/cdn/shop/products/PeakyBlinders-NetflixTVShow-ArtPoster_125897c4-6348-41e8-b195-d203700ebcca.jpg?v=1619864555",

                    width: 150,
                    height: 220,
                  ),
                ),
                
              ],
            ),
            ),
            SizedBox(
                height: 20,
            ),
            Text("Most Popular"),
            SingleChildScrollView(scrollDirection: Axis.horizontal,
            child:Row(
              children: [
                Container(
                  color: Colors.white,
                  child: Image.network(
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR0kR0gMemRl9ylPTzmmuQQVb10vo8n7kXL7BeHkeo_4lmJS56C8-WKIy_GYK12wnEmPlc",

                    width: 150,
                    height: 220,
                  ),
                ),
                Container(
                  color: Colors.white,
                  child: Image.network(
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZ5Cq8kozpWIaq5Aohw4rjKkh_eE7nUkDV5zcHClQaYw&s",

                    width: 150,
                    height: 220,
                  ),
                ),
                Container(
                  color: Colors.white,
                  child: Image.network(
                    "https://dbdzm869oupei.cloudfront.net/img/posters/preview/91008.png",

                    width: 150,
                    height: 220,
                  ),
                ),
                
              ],
            ),
            ),
          ],
          
         ) 
        ],
      ),
    );
  }
}