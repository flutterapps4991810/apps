import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: ToDoList(),
    );
  }
}

class ToDoList extends StatefulWidget {
  const ToDoList({super.key});

  @override
  State<ToDoList> createState() => _ToDoListState();
}

class _ToDoListState extends State<ToDoList> {
  String? formatDate;
  void showBottomSheet() {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return Padding(
          padding: MediaQuery.of(context).viewInsets,
          // padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(
                height: 7,
              ),
              Text(
                "Create Task",
                style: GoogleFonts.quicksand(
                    textStyle: const TextStyle(
                        fontSize: 22, fontWeight: FontWeight.w600)),
              ),
              const SizedBox(
                height: 7,
              ),
              TextField(
                controller: titleController,
                decoration: InputDecoration(
                  // enabledBorder: OutlineInputBorder(),
                  labelText: 'Title :',
                  labelStyle: GoogleFonts.quicksand(
                      textStyle: const TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 13,
                          color: Color.fromRGBO(0, 139, 148, 1))),
                  enabledBorder: const OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Color.fromRGBO(0, 139, 148, 1)),
                    borderRadius: BorderRadius.all(
                      Radius.circular(16),
                    ),
                  ),
                  focusedBorder: const OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Color.fromRGBO(0, 139, 148, 1)),
                    borderRadius: BorderRadius.all(
                      Radius.circular(16),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              TextField(
                controller: despController,
                maxLines: 3,
                decoration: InputDecoration(
                  // enabledBorder: OutlineInputBorder(),
                  labelText: 'Description :',
                  labelStyle: GoogleFonts.quicksand(
                      textStyle: const TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 13,
                          color: Color.fromRGBO(0, 139, 148, 1))),
                  enabledBorder: const OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Color.fromRGBO(0, 139, 148, 1)),
                    borderRadius: BorderRadius.all(
                      Radius.circular(16),
                    ),
                  ),
                  focusedBorder: const OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Color.fromRGBO(0, 139, 148, 1)),
                    borderRadius: BorderRadius.all(
                      Radius.circular(16),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              TextField(
                controller: dateController,
                decoration: InputDecoration(
                  suffixIcon: InkWell(
                    onTap: () async {
                      DateTime? pickerDate = await showDatePicker(
                        context: context,
                        initialDate: DateTime(2023),
                        firstDate: DateTime(2021),
                        lastDate: DateTime.now(),
                      );
                      String? formatDate =
                          DateFormat.yMMMMd().format(pickerDate!);
                      setState(() {
                        dateController.text = formatDate;
                      });
                    },
                    child: const Icon(
                      Icons.calendar_month_outlined,
                      color: Color.fromRGBO(0, 139, 148, 1),
                      size: 20,
                    ),
                  ),
                  // enabledBorder: OutlineInputBorder(),
                  labelText: 'Date :',
                  labelStyle: GoogleFonts.quicksand(
                      textStyle: const TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 13,
                          color: Color.fromRGBO(0, 139, 148, 1))),
                  enabledBorder: const OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Color.fromRGBO(0, 139, 148, 1)),
                    borderRadius: BorderRadius.all(
                      Radius.circular(16),
                    ),
                  ),
                  focusedBorder: const OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Color.fromRGBO(0, 139, 148, 1)),
                    borderRadius: BorderRadius.all(
                      Radius.circular(16),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  shadowColor: const Color.fromRGBO(0, 0, 0, 0.1),

                  minimumSize: const Size(300, 50),

                  // fixedSize: Size.fromWidth(300),
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12))),
                  backgroundColor: const Color.fromRGBO(2, 167, 177, 1),
                  foregroundColor: Colors.white,
                ),
                onPressed: () {
                  setState(() {
                    titleList.add(titleController.text);
                    descriptionList.add(despController.text);
                    dateList.add(dateController.text);
                    Navigator.of(context).pop();
                  });
                },
                child: Text(
                  "Submit",
                  style: GoogleFonts.inter(
                    textStyle: const TextStyle(
                        fontSize: 20, fontWeight: FontWeight.w700),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  List colorList = [
    const Color.fromRGBO(250, 232, 232, 1),
    const Color.fromRGBO(232, 237, 250, 1),
    const Color.fromRGBO(250, 249, 232, 1),
    const Color.fromRGBO(250, 232, 250, 1),
    const Color.fromRGBO(250, 232, 232, 1),
  ];

  // List cardList = [
  //   TodoModalClass(title: title, description: description, date: date)
  // ];
  List titleList = [];
  List descriptionList = [];
  List dateList = [];
  final titleController = TextEditingController();
  final despController = TextEditingController();
  final dateController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      // floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
      floatingActionButton: FloatingActionButton(
        // splashColor: Colors.yellow,
        // hoverColor: Colors.red,
        hoverElevation: 4,
        backgroundColor: const Color.fromRGBO(0, 139, 148, 1),
        foregroundColor: Colors.white,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(46))),
        onPressed: showBottomSheet,
        child: const Icon(
          Icons.add,
          size: 50,
        ),
      ),
      appBar: AppBar(
        title: Text(
          "To-do-list",
          style: GoogleFonts.quicksand(
            textStyle: const TextStyle(
              fontSize: 28,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        backgroundColor: const Color.fromRGBO(2, 167, 177, 1),
        foregroundColor: Colors.white,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        child: ListView.builder(
          itemCount: titleList.length,
          itemBuilder: (context, index) {
            return Container(
              height: 112,
              width: 330,
              child: Card(
                elevation: 5,
                color: colorList[index % colorList.length],
                child: Column(
                  children: [
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 4.0),
                          child: Column(
                            children: [
                              Container(
                                height: 52,
                                width: 52,
                                decoration: const BoxDecoration(boxShadow: [
                                  BoxShadow(
                                      color: Colors.black,
                                      offset: Offset(1, 1),
                                      blurRadius: 2)
                                ], shape: BoxShape.circle, color: Colors.white),
                                child: SizedBox(
                                  height: 23.79,
                                  width: 19.07,
                                  child: Image.network(
                                      'https://cdn.icon-icons.com/icons2/1526/PNG/512/checklist_106575.png'),
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Column(
                          children: [
                            const SizedBox(
                              height: 10,
                            ),
                            SizedBox(
                              height: 20,
                              width: 250,
                              child: Text(
                                titleList[index],
                                style: GoogleFonts.quicksand(
                                    textStyle: const TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 12)),
                              ),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            SizedBox(
                              height: 50,
                              width: 250,
                              child: Text(
                                descriptionList[index],
                                style: GoogleFonts.quicksand(
                                    textStyle: const TextStyle(
                                        fontSize: 10,
                                        fontWeight: FontWeight.w500)),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10, right: 10),
                      child: Row(
                        children: [
                          Text(
                            dateList[index],
                            style: GoogleFonts.quicksand(
                                textStyle: const TextStyle(
                                    fontWeight: FontWeight.w500, fontSize: 10)),
                          ),
                          const Spacer(),
                          SizedBox(
                            height: 13,
                            width: 13,
                            child: InkWell(
                              onTap: () {
                                showModalBottomSheet(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: SingleChildScrollView(
                                        child: Column(
                                          children: [
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              "Create Task",
                                              style: GoogleFonts.quicksand(
                                                  textStyle: const TextStyle(
                                                      fontSize: 22,
                                                      fontWeight:
                                                          FontWeight.w600)),
                                            ),
                                            const SizedBox(
                                              height: 20,
                                            ),
                                            TextField(
                                              controller: titleController,
                                              decoration: InputDecoration(
                                                // enabledBorder: OutlineInputBorder(),
                                                labelText: 'Title :',
                                                labelStyle:
                                                    GoogleFonts.quicksand(
                                                        textStyle:
                                                            const TextStyle(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontSize: 13,
                                                                color: Color
                                                                    .fromRGBO(
                                                                        0,
                                                                        139,
                                                                        148,
                                                                        1))),
                                                enabledBorder:
                                                    const OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Color.fromRGBO(
                                                          0, 139, 148, 1)),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                    Radius.circular(16),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            TextField(
                                              controller: despController,
                                              maxLines: 3,
                                              decoration: InputDecoration(
                                                // enabledBorder: OutlineInputBorder(),
                                                labelText: 'Description :',
                                                labelStyle:
                                                    GoogleFonts.quicksand(
                                                        textStyle:
                                                            const TextStyle(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontSize: 13,
                                                                color: Color
                                                                    .fromRGBO(
                                                                        0,
                                                                        139,
                                                                        148,
                                                                        1))),
                                                enabledBorder:
                                                    const OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Color.fromRGBO(
                                                          0, 139, 148, 1)),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                    Radius.circular(16),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            TextField(
                                              controller: dateController,
                                              decoration: InputDecoration(
                                                suffixIcon: InkWell(
                                                  onTap: () async {
                                                    DateTime? pickerDate =
                                                        await showDatePicker(
                                                            context: context,
                                                            initialDate:
                                                                DateTime.now(),
                                                            firstDate: DateTime(
                                                                Int32x4.yyww),
                                                            lastDate: DateTime(
                                                                Int32x4.yyww));
                                                    String formatDate =
                                                        DateFormat.yMMM()
                                                            .format(
                                                                pickerDate!);
                                                  },
                                                  child: const Icon(
                                                    Icons
                                                        .calendar_month_outlined,
                                                    color: Color.fromRGBO(
                                                        0, 139, 148, 1),
                                                    size: 20,
                                                  ),
                                                ),
                                                // enabledBorder: OutlineInputBorder(),
                                                labelText: 'Date :',
                                                labelStyle:
                                                    GoogleFonts.quicksand(
                                                        textStyle:
                                                            const TextStyle(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontSize: 13,
                                                                color: Color
                                                                    .fromRGBO(
                                                                        0,
                                                                        139,
                                                                        148,
                                                                        1))),
                                                enabledBorder:
                                                    const OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Color.fromRGBO(
                                                          0, 139, 148, 1)),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                    Radius.circular(16),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 20,
                                            ),
                                            ElevatedButton(
                                              style: ElevatedButton.styleFrom(
                                                shadowColor: const Color.fromRGBO(
                                                    0, 0, 0, 0.1),

                                                minimumSize: const Size(300, 50),

                                                // fixedSize: Size.fromWidth(300),
                                                shape:
                                                    const RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius.all(
                                                                Radius.circular(
                                                                    12))),
                                                backgroundColor: const Color.fromRGBO(
                                                    2, 167, 177, 1),
                                                foregroundColor: Colors.white,
                                              ),
                                              onPressed: () {
                                                setState(() {
                                                
                                                  titleList[index] =
                                                      titleController.text;
                                                  descriptionList[index] =
                                                      despController.text;
                                                  dateList[index] =
                                                      dateController.text;
                                                  Navigator.of(context).pop();
                                                  titleController.clear();
                                                  despController.clear();
                                                  dateController.clear();
                                                  
                                                });
                                              },
                                              child: Text(
                                                "Submit",
                                                style: GoogleFonts.inter(
                                                  textStyle: const TextStyle(
                                                      fontSize: 20,
                                                      fontWeight:
                                                          FontWeight.w700),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                );

                                print("Edit");
                              },
                              child: const Icon(
                                Icons.edit_outlined,
                                size: 15,
                                color: Color.fromRGBO(0, 139, 148, 1),
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          SizedBox(
                            height: 13,
                            width: 13,
                            child: InkWell(
                              onTap: () {
                                setState(() {
                                  // titleList[index] = '';
                                  // descriptionList[index] = '';
                                  // dateList[index] = '';
                                  titleList.removeAt(index);
                                });
                                print("Delete");
                              },
                              child: const Icon(
                                Icons.delete_outline_outlined,
                                size: 15,
                                color: Color.fromRGBO(0, 139, 148, 1),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}

class TodoModalClass {
  String title;
  String description;
  String date;
  TodoModalClass(
      {required this.title, required this.description, required this.date});
}