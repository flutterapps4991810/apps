import 'package:flutter/material.dart';

class assignment1 extends StatefulWidget {
  const assignment1({super.key});

  @override
  State<assignment1> createState() => _assignment1();
}

class _assignment1 extends State<assignment1> {

  int quecount=1;

  void counter(){
    setState(() {
      quecount++;
    },);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.pink,
        title: const Text("Tech Quiz"),
        titleTextStyle: const TextStyle(
            fontSize: 25, color: Colors.white, fontWeight: FontWeight.bold),
      ),
      body: Center(
        child: Column(children: [
          const SizedBox(
            height: 30,
          ),
          const Text("Question"),
          Text(
            "$quecount/10"
          ),
          const SizedBox(
            height: 30,
          ),
          const Text("Question 1: What s Flutter?"),
          const SizedBox(
            height: 40,
          ),
          ElevatedButton(
            onPressed: () {},
            style: const ButtonStyle(
              backgroundColor: MaterialStatePropertyAll(Colors.blue),
              fixedSize: MaterialStatePropertyAll(Size(350, 10)),
            ),
            child: const Text("Option1",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 15)),
          ),
          const SizedBox(
            height: 20,
          ),
          ElevatedButton(
            onPressed: () {},
            style: const ButtonStyle(
              backgroundColor: MaterialStatePropertyAll(Colors.blue),
              fixedSize: MaterialStatePropertyAll(Size(350, 10)),
            ),
            child: const Text("Option2",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 15)),
          ),
          const SizedBox(
            height: 20,
          ),
          ElevatedButton(
            onPressed: () {},
            style: const ButtonStyle(
                backgroundColor: MaterialStatePropertyAll(Colors.blue),
                fixedSize: MaterialStatePropertyAll(Size(350, 10))),
            child: const Text("Option3",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 15)),
          ),
          const SizedBox(
            height: 20,
          ),
          ElevatedButton(
            onPressed: () {},
            style: const ButtonStyle(
              backgroundColor: MaterialStatePropertyAll(Colors.blue),
              fixedSize: MaterialStatePropertyAll(Size(350, 10)),
            ),
            child: const Text(
              "Option4",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 15),
            ),
            //style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold)),],
          ),
        ]),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            counter();
          },
           child: const Text("Next"),
      ),
    );
  }
}
