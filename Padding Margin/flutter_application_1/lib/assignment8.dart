import 'package:flutter/material.dart';

class Assignment8 extends StatefulWidget {
  const Assignment8({super.key});

  @override
  State<Assignment8> createState ()=> _Assigment8State();
}

class _Assigment8State extends State<Assignment8> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Padding and Margin"
        ),
        backgroundColor: Colors.deepPurple,
      ),

      body: Center(
        child: Container(
          color: Colors.blue,
          child: Container(
            height: 250,
            width: 250,
            color: Colors.amber,
            alignment: Alignment.center,
            padding: const EdgeInsets.all(20),
            margin: const EdgeInsets.all(20),
            child: Image.network(
              "https://static-cse.canva.com/blob/825910/ComposeStunningImages6.jpg"
            ),
          ),          
        )
        ),
    );
  }
}
