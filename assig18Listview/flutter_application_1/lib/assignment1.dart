import 'package:flutter/material.dart';

void main() {
  runApp(const assignment1());
}

class assignment1 extends StatefulWidget {
  const assignment1({super.key});

  @override
  State createState() => _assignment1();
}
class _assignment1 extends State<assignment1>{

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text("List View"),
        ),
        body: ListView(
          children: [
            Container(
              margin: const EdgeInsets.all(20),
              child: Image.network("https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg",
              height: 400,
              width: 400,
              ),
            ),
            Container(
              margin: const EdgeInsets.all(20),
              child: Image.network("https://images.pexels.com/photos/268533/pexels-photo-268533.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
              height: 400,
              width: 400,
              ),
            ),
            Container(
              margin: EdgeInsets.all(20),
              child: Image.network("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTmxxiiyvmAZJovRUKSzYkhXcZ-A6mxsDj3XkfHPWuasg&s",
              height: 400,
              width:400,
              ),
            )
          ],
        )
      ),
    );
  }
}