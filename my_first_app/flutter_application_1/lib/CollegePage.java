import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class CollegePage extends Application {
    
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        // Create a StackPane as the root node
        StackPane root = new StackPane();

        // Create a pink background rectangle
        Rectangle pinkBackground = new Rectangle(800, 600);
        pinkBackground.setFill(Color.PINK);

        // Create a green "COLLEGE" text
        Text collegeText = new Text("COLLEGE");
        collegeText.setFont(Font.font("Arial", FontWeight.BOLD, 48));
        collegeText.setFill(Color.GREEN);

        // Add the pink background and the "COLLEGE" text to the StackPane
        root.getChildren().addAll(pinkBackground, collegeText);

        // Create a Scene
        Scene scene = new Scene(root, 800, 600);

        // Set the title of the stage
        primaryStage.setTitle("College Page");

        // Set the scene for the stage
        primaryStage.setScene(scene);

        // Show the stage
        primaryStage.show();
    }

    
}
