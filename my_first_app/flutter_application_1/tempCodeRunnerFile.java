import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.paint.Color;

public class CollegePage extends Application {

    @Override
    public void start(Stage primaryStage) {
        // Create a StackPane as the root container
        StackPane root = new StackPane();

        // Set the background color to pink
        root.setStyle("-fx-background-color: pink;");

        // Create the "COLLEGE" text
        Text collegeText = new Text("COLLEGE");
        collegeText.setFont(Font.font("Arial", 48)); // Set the font size to make it bigger
        collegeText.setFill(Color.GREEN); // Set the text color to green

        // Add the text to the StackPane
        root.getChildren().add(collegeText);

        // Create the scene
        Scene scene = new Scene(root, 400, 200);

        // Set the title of the window
        primaryStage.setTitle("College Page");

        // Set the scene for the stage
        primaryStage.setScene(scene);

        // Show the stage
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
