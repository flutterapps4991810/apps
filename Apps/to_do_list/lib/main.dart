import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LoginPage(),
    );
  }
}

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController userNameTextEditingController = TextEditingController();
  TextEditingController passwordTextEditingController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final String validUsername = "Sachin";
  final String validPassword = "340710";

  bool _isPasswordVisible = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 224, 175, 191),
      
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(height: 20),
              Image.network(
                "https://tse3.mm.bing.net/th?id=OIP.uqi4Pm5s5qLc2GCCtZ7_ygHaHa&pid=Api&P=0&h=180",
                width: 90,
                height: 90,
              ),
              const SizedBox(height: 20),
              TextFormField(
                controller: userNameTextEditingController,
                decoration: InputDecoration(
                  hintText: "Enter username",
                  labelText: "Enter username",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  prefixIcon: const Icon(Icons.person),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Please enter username";
                  } else if (value != validUsername) {
                    return "Invalid username";
                  } else {
                    return null;
                  }
                },
                keyboardType: TextInputType.emailAddress,
              ),
              const SizedBox(height: 20),
              TextFormField(
                controller: passwordTextEditingController,
                obscureText: !_isPasswordVisible,
                obscuringCharacter: "*",
                decoration: InputDecoration(
                  hintText: "Enter password",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  labelText: "Enter password",
                  prefixIcon: const Icon(Icons.lock),
                  suffixIcon: IconButton(
                    icon: Icon(_isPasswordVisible ? Icons.visibility : Icons.visibility_off),
                    onPressed: () {
                      setState(() {
                        _isPasswordVisible = !_isPasswordVisible;
                      });
                    },
                  ),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Please enter password";
                  } else if (value != validPassword) {
                    return "Invalid password";
                  } else {
                    return null;
                  }
                },
              ),
              const SizedBox(height: 20),
              ElevatedButton(
                onPressed: () {
                  bool loginValidated = _formKey.currentState!.validate();
                  if (loginValidated) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: Text("Login Successful"),
                      ),
                    );
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const ToDoList(),
                      ),
                    );
                  } else {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: Text("Login Failed"),
                      ),
                    );
                  }
                },
                child: const Text("Login",selectionColor: Colors.black,),
                style: ButtonStyle(backgroundColor: MaterialStateColor.resolveWith((states) => Colors.greenAccent),),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class ToDoList extends StatefulWidget {
  const ToDoList({Key? key}) : super(key: key);

  @override
  State<ToDoList> createState() => _ToDoListState();
}

class TodoModalClass {
  String title;
  String description;
  String date;
  TodoModalClass({
    required this.title,
    required this.description,
    required this.date,
  });
}

class _ToDoListState extends State<ToDoList> {
  bool isEditing = false;
  String? formatDate;
  final titleController = TextEditingController();
  final despController = TextEditingController();
  final dateController = TextEditingController();

  List<Color> colorList = [
    const Color.fromRGBO(250, 232, 232, 1),
    const Color.fromRGBO(232, 237, 250, 1),
    const Color.fromRGBO(250, 249, 232, 1),
    const Color.fromRGBO(250, 232, 250, 1),
    const Color.fromRGBO(250, 232, 232, 1),
    const Color.fromRGBO(252, 232, 232, 1),
    const Color.fromRGBO(233, 237, 250, 1),
    const Color.fromRGBO(255, 249, 232, 1),
    const Color.fromRGBO(256, 232, 250, 1),
    const Color.fromRGBO(257, 232, 232, 1),
  ];

  List<String> titleList = [];
  List<String> descriptionList = [];
  List<String> dateList = [];

  void showBottomSheet(int? index) {
    if (isEditing && index != null) {
      titleController.text = titleList[index];
      despController.text = descriptionList[index];
      dateController.text = dateList[index];
    }

    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(height: 10),
                Text(
                  "Create Task",
                  style: GoogleFonts.quicksand(
                    textStyle: const TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                TextField(
                  controller: titleController,
                  decoration: InputDecoration(
                    labelText: 'Title :',
                    labelStyle: GoogleFonts.quicksand(
                      textStyle: const TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 13,
                        color: Color.fromRGBO(0, 139, 148, 1),
                      ),
                    ),
                    enabledBorder: const OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromRGBO(0, 139, 148, 1),
                      ),
                      borderRadius: BorderRadius.all(
                        Radius.circular(16),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                TextField(
                  controller: despController,
                  maxLines: 3,
                  decoration: InputDecoration(
                    labelText: 'Description :',
                    labelStyle: GoogleFonts.quicksand(
                      textStyle: const TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 13,
                        color: Color.fromRGBO(0, 139, 148, 1),
                      ),
                    ),
                    enabledBorder: const OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromRGBO(0, 139, 148, 1),
                      ),
                      borderRadius: BorderRadius.all(
                        Radius.circular(16),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                GestureDetector(
                  child: TextFormField(
                    controller: dateController,
                    readOnly: true,
                    decoration: InputDecoration(
                      suffixIcon: InkWell(
                        onTap: () async {
                          DateTime? pickedDate = await showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(2021),
                            lastDate: DateTime(2025),
                          );
                          if (pickedDate != null) {
                            setState(() {
                              formatDate = DateFormat.yMMMMd().format(pickedDate);
                              dateController.text = formatDate!;
                            });
                          }
                        },
                        child: const Icon(
                          Icons.calendar_today,
                          color: Color.fromARGB(255, 124, 238, 139),
                        ),
                      ),
                      labelText: 'Date :',
                      labelStyle: GoogleFonts.quicksand(
                        textStyle: const TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 13,
                          color: Color.fromRGBO(0, 139, 148, 1),
                        ),
                      ),
                      enabledBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color.fromRGBO(0, 139, 148, 1),
                        ),
                        borderRadius: BorderRadius.all(
                          Radius.circular(16),
                        ),
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color.fromRGBO(0, 139, 148, 1),
                        ),
                        borderRadius: BorderRadius.all(
                          Radius.circular(16),
                        ),
                      ),
                    ),
                  ),
                  onTap: () async {},
                ),
                const SizedBox(height: 20),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    shadowColor: const Color.fromRGBO(0, 0, 0, 0.1),
                    minimumSize: const Size(300, 50),
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(12),
                      ),
                    ),
                    backgroundColor: const Color.fromARGB(255, 124, 238, 139),
                    foregroundColor: Colors.white,
                  ),
                  onPressed: () {
                    if (titleController.text.trim().isNotEmpty &&
                        despController.text.trim().isNotEmpty &&
                        dateController.text.trim().isNotEmpty) {
                      if (isEditing) {
                        titleList[index!] = titleController.text;
                        descriptionList[index] = despController.text;
                        dateList[index] = dateController.text;
                        isEditing = false;
                      } else {
                        titleList.add(titleController.text);
                        descriptionList.add(despController.text);
                        dateList.add(dateController.text);
                      }
                      setState(() {
                        Navigator.of(context).pop();
                        titleController.clear();
                        despController.clear();
                        dateController.clear();
                      });
                    } else {
                      showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                          title: const Text("Error"),
                          content: const Text("Please fill all fields."),
                          actions: [
                            TextButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: const Text("OK"),
                            ),
                          ],
                        ),
                      );
                    }
                  },
                  child: Text(
                    "Submit",
                    style: GoogleFonts.inter(
                      textStyle: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        hoverElevation: 4,
        backgroundColor: const Color.fromARGB(255, 124, 238, 139),
        foregroundColor: Colors.white,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(46),
          ),
        ),
        onPressed: () => showBottomSheet(null),
        child: const Icon(
          Icons.add,
          size: 50,
        ),
      ),
      appBar: AppBar(
        title: Text(
          "To-do List",
          style: GoogleFonts.quicksand(
            textStyle: const TextStyle(
              fontSize: 28,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        centerTitle: true,
        backgroundColor: const Color.fromARGB(255, 124, 238, 139),
        foregroundColor: Colors.white,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        child: ListView.builder(
          itemCount: titleList.length,
          itemBuilder: (context, index) {
            return SizedBox(
              height: 112,
              width: 330,
              child: Card(
                elevation: 5,
                color: colorList[index % colorList.length],
                child: Column(
                  children: [
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 4.0),
                          child: Column(
                            children: [
                              Container(
                                height: 52,
                                width: 52,
                                decoration: const BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black,
                                      offset: Offset(1, 1),
                                      blurRadius: 2,
                                    )
                                  ],
                                  shape: BoxShape.circle,
                                  color: Colors.white,
                                ),
                                child: SizedBox(
                                  height: 23.79,
                                  width: 19.07,
                                  child: Image.network(
                                    'https://cdn-icons-png.flaticon.com/512/3135/3135715.png',
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(width: 10),
                        Column(
                          children: [
                            const SizedBox(height: 10),
                            SizedBox(
                              height: 20,
                              width: 250,
                              child: Text(
                                titleList[index],
                                style: GoogleFonts.quicksand(
                                  textStyle: const TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(height: 5),
                            SizedBox(
                              height: 50,
                              width: 250,
                              child: Text(
                                descriptionList[index],
                                style: GoogleFonts.quicksand(
                                  textStyle: const TextStyle(
                                    fontSize: 10,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10, right: 10),
                      child: Row(
                        children: [
                          Text(
                            dateList[index],
                            style: GoogleFonts.quicksand(
                              textStyle: const TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 10,
                              ),
                            ),
                          ),
                          const Spacer(),
                          SizedBox(
                            height: 13,
                            width: 13,
                            child: InkWell(
                              onTap: () {
                                isEditing = true;
                                showBottomSheet(index);
                              },
                              child: const Icon(
                                Icons.edit_outlined,
                                size: 15,
                                color: Color.fromRGBO(0, 139, 148, 1),
                              ),
                            ),
                          ),
                          const SizedBox(width: 10),
                          SizedBox(
                            height: 13,
                            width: 13,
                            child: InkWell(
                              onTap: () {
                                setState(() {
                                  titleList.removeAt(index);
                                });
                              },
                              child: const Icon(
                                Icons.delete_outline_outlined,
                                size: 15,
                                color: Color.fromRGBO(0, 139, 148, 1),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}