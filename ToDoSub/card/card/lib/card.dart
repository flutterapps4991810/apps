// ignore_for_file: sort_child_properties_last

import 'package:flutter/material.dart';

void main() {
  runApp(const card());
}



class card extends StatefulWidget {
  const card({super.key});

  @override
  State<card> createState() => _card();
}
class _card extends State<card> {
  List cardColorList = [
    const Color.fromRGBO(250, 232, 232, 1),
    const Color.fromRGBO(232, 237, 250, 1),
    const Color.fromRGBO(250, 249, 232, 1),
    const Color.fromRGBO(250, 232, 250, 1),
    //const Color.fromRGBO(250, 232, 250, 1),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(8),
        child: ListView.builder(
            itemCount: cardColorList.length,
            itemBuilder: (context, index) {
              return Container(
                margin: const EdgeInsets.all(10),
                width: 330,
                decoration: BoxDecoration(
                  boxShadow: const [BoxShadow(color : Color.fromRGBO(0, 0, 0, 0.1), offset: Offset(0, 10), blurRadius: 20, spreadRadius: 1)],
                  borderRadius: BorderRadius.circular(10),
                  color: cardColorList[index],
                ),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Padding (
                          child: Container(
                           child: Image.network(
                                "https://tse2.mm.bing.net/th?id=OIP.FUpsI3xTi6WztyFWVQ5owgHaHw&pid=Api&P=0&h=180"),
                            height: 52,
                            width: 52,
                            decoration: const BoxDecoration(
                              color: Color.fromRGBO(255, 255, 255, 1),
                              shape: BoxShape.circle,
                              boxShadow: [
                                BoxShadow(
                                    color: Color.fromARGB(124, 67, 67, 45),
                                    offset: Offset(0, 0),
                                    blurRadius: 10),
                              ],
                            ),
                          ),
                          padding:const  EdgeInsets.all(30),
                        ),
                        const Expanded(child: Column(
                          children: [
                            Row(
                              children: [
                                Text ("Lorem Ipsum is simply setting industry. ", style: TextStyle(fontWeight: FontWeight.bold),),
                              ],
                            ),
                            Text ("Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s", style: TextStyle(fontWeight: FontWeight.normal),),
                          ],
                        ),)
                      ],
                    ),
                    Row(
                        children: [
                          SizedBox(width: 20,),
                          Text("10/10/2023"),
                          Spacer(),
                          IconButton(onPressed: () {}, icon: Icon(Icons.edit_outlined),),
                          IconButton(onPressed: () {}, icon: Icon(Icons.delete,),),],
                      ),
                    
                
                  ],
                ),
              
                
              );
              
            }),

      ),
      floatingActionButton: FloatingActionButton(
        onPressed:() {
          showModalBottomSheet(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(top: Radius.circular(10))
            ),
        
  
            context: context, builder: (context){
            return Container(
            
            
            ); 
          },
          );
        },
        child: const Icon(Icons.add),
       ),
    );
  }
}